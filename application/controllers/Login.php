<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent :: __construct();
		$this->load->model('login_model');
    }
	public function index()
	{	
		$headData['page_name']='login';
		$this->load->view('login',$headData);
	}

	public function check()
	{
		$postData=$this->input->post();
		$result=$this->login_model->checkLogin($postData);
		if($result['status']==1)
		{
			redirect( '/products' , 'refresh' );
		}else{
			$headData['page_name']='login';
			$headData['error']=$result['message'];
			$this->load->view('login',$headData);
		}
	}
	public function get_company()
	{
		$postData=$this->input->post();
		$getCompanies=$this->login_model->get_company($postData);	
		echo $getCompanies;
	}
}
