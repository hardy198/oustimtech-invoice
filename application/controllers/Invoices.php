<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends CI_Controller {

	public function __construct()
	{
		parent :: __construct();
		if(!$this->session->userdata('isLogin')){
			redirect( '/login' , 'refresh' );
		}
		$this->load->model('invoice_model');
		$this->load->model('general_model');
    }
	public function index()
	{	//$this->session->unset_userdata('carts');
		$headData['page_name']='invoices';
		$headData['custom_js']=array('invoice.js');
		$data['invoices']=$this->invoice_model->get_invoices();

		$this->load->view('inc/head',$headData);
		$this->load->view('invoice/list',$data);
		$this->load->view('inc/footer',$headData);
	}
	public function save()
	{
		$postData=$this->input->post();
		$results=$this->invoice_model->save($postData);
		echo json_encode($results);
	}
	public function lists()
	{
		$records=$this->invoice_model->lists();
		echo json_encode($records);
	}
	public function delete()
	{	
		$action_id=$this->input->post('action_id');
		$records=$this->invoice_model->delete($action_id);
		echo json_encode($records);
	}
	public function get()
	{
		$action_id=$this->input->post('action_id');

		if($action_id!=''){
			$results=$this->invoice_model->get($action_id);
			$data['postData']=$results['master_detail'];
			$data['details']=$results['details'];
		}

		$data['action_id']=$action_id;
		$data['mode']=$this->input->post('mode');
		$data['taxes']=$this->general_model->get_taxes();
		$data['uoms']=$this->general_model->get_uom();
		$data['products']=$this->general_model->get_products();
		$data['customers']=$this->general_model->get_customers();
		$source=$this->load->view('invoice/add',$data,TRUE);
		echo $source;
	}
	public function get_shipping_address()
	{
		$action_id=$this->input->post('action_id');
		$shipping_details=$this->general_model->get_shipping_address($action_id);
		echo json_encode($shipping_details);
	}
	public function add_to_cart()
	{	$this->load->helper('string');
		$rowId=$this->input->post('rowId');
		if($rowId==''){ $rowId=random_string('alnum', 16); }
		
		$postData=$this->input->post();
		$taxes=$this->general_model->get_taxe_value($postData['tax_id']);
		$postData['tax_rate']=$taxes->tax_rate;
		$postData['cgst']=$taxes->cgst;
		$postData['sgst']=$taxes->sgst;
		$postData['igst']=$taxes->igst;

		$session_data=$this->session->userdata('carts');
		if(count($session_data) >0 ){
			$session_data[$rowId]=$postData; 
			$this->session->set_userdata("carts", $session_data);
		}
		else{ $this->session->set_userdata("carts", array($rowId=>$postData));  }
		
		//$data['items']=$this->session->userdata('carts');
		$source=$this->load->view('invoice/cart','',TRUE);
		echo $source;
	}
	public function get_cart_item()
	{
		$rowId=$this->input->post('rowId');
		$carts=$this->session->userdata('carts');
		echo json_encode($carts[$rowId]);
	}
	public function remove_cart_item()
	{
		$rowId=$this->input->post('rowId');
		$carts=$this->session->userdata('carts');
		unset($carts[$rowId]);
		$this->session->set_userdata("carts", $carts);
	}
	public function get_cart_total()
	{
		$source=$this->load->view('invoice/cart_total','',TRUE);
		echo $source;
	}
	public function add_shipping()
	{
		$postData=$this->input->post();
		$sessionData=array();
		if(isset($postData['shipping_cost']) && $postData['shipping_cost']!='')
		{
			$sessionData['shipping_cost']=$postData['shipping_cost'];
		}
		if(isset($postData['shipping_tax']) && $postData['shipping_tax']!='')
		{	if($postData['shipping_tax']!=0){
				$taxes=$this->general_model->get_taxe_value($postData['shipping_tax']);
				$sessionData['shipping_tax_rate']=$taxes->tax_rate;
				$sessionData['shipping_cgst']=$taxes->cgst;
				$sessionData['shipping_sgst']=$taxes->sgst;
				$sessionData['shipping_igst']=$taxes->igst;	
			}else{
				$sessionData['shipping_tax_rate']=0;
				$sessionData['shipping_cgst']=0;
				$sessionData['shipping_sgst']=0;
				$sessionData['shipping_igst']=0;	
			}
		}else{
			$sessionData['shipping_tax_rate']=0;
			$sessionData['shipping_cgst']=0;
			$sessionData['shipping_sgst']=0;
			$sessionData['shipping_igst']=0;
		}
		$this->session->set_userdata("shipping_charge", $sessionData);
	}
	public function add_cart_discount()
	{	$postData=$this->input->post();
		$sessionData=array();
		if(isset($postData['discount_cost']) && $postData['discount_cost']!='')
		{
			$sessionData['discount_cost']=$postData['discount_cost'];
		}
		if(isset($postData['discount_type']))
		{	
			$sessionData['discount_type']=$postData['discount_type'];
		}
		$this->session->set_userdata("cart_discount", $sessionData);
	}
	public function preview_invoice()
	{
		$source=$this->load->view('invoice/preview_invoice','',TRUE);
		echo $source;
	}
	public function generate_pdf()
	{	
		$html=$this->load->view('invoice/preview_invoice','',TRUE);
		$apikey = '9b649fbf-d31c-4f45-bdd3-2724a2110034';
		 
		// Convert the HTML string to a PDF using those parameters.  Note if you have a very long HTML string use POST rather than get.  See example #5
		$postdata = http_build_query(array('apikey' => $apikey,'value' => $html,'MarginBottom' => '30','MarginTop' => '20'));
		$opts = array('http' =>array('method'  => 'POST','header'  => 'Content-type: application/x-www-form-urlencoded','content' => $postdata));
	 
		$context  = stream_context_create($opts);	 
		// Convert the HTML string to a PDF using those parameters
		$result = file_get_contents('http://api.html2pdfrocket.com/pdf', false, $context);

		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename=' . 'alias-name.pdf' );
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . strlen($result));
		echo $result;
	}

	public function importproduct()
	{	//print_r($_FILES);
		$this->load->library('excel');
		$this->load->model('excel_model');

		$configUpload['upload_path'] = 'uploads/excel/';
     	$configUpload['allowed_types'] = 'xls|xlsx|csv';
     	$this->load->library('upload', $configUpload);
     	if ( ! $this->upload->do_upload('excelfile'))
		{
			$error = array('message' => $this->upload->display_errors(),"status"=>0);
			echo json_encode($error);
			exit();
		}
     	$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
     	$file_name = $upload_data['file_name']; //uploded file name
	 	$extension=$upload_data['file_ext'];    // uploded file extension
	 	
	 	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	 	//$objReader= PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007 	  
        //$objPHPExcel = PHPExcel_IOFactory::load("c:\cctv.xls");  // Remove the createReader line before this
        $objReader->setReadDataOnly(true); 		  
		$objPHPExcel=$objReader->load('uploads/excel/'.$file_name);		 
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0); 
		$excelData=array();
		for($i=2;$i<=$totalrows;$i++)
        {
              $product_name= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
              $price= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
              $uom= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
              $tax= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
              $description= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
              $qty= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
              $type= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
              $hsn_scn= $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
              $sku= $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
              $batch_no= $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();

              array_push($excelData, array("product_name"=>$product_name,"price"=>$price,"uom"=>$uom,"tax"=>$tax,"description"=>$description,
              							   "qty"=>$qty,"type"=>$type,"hsn_scn"=>$hsn_scn,"sku"=>$sku,"batch_no"=>$batch_no));
        }
        $returnArray=$this->excel_model->import_invoices($excelData);
        //print_r($returnArray);

       unlink('uploads/excel/'.$file_name);
	   $success = array('message' => 'Import process is in progress.',"status"=>1);
	   echo json_encode($success);
	}

	public function exportproduct()
	{
		$this->load->library('excel');
		$results=$this->invoice_model->get_all_product();
		$fields=$results['fields'];
		$rows=$results['rows'];

		$objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
 
        // Field names in the first row
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach($rows as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="invoices_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');
	}

	public function generate_invoice_prefix()
	{
		$this->db->query("CREATE TRIGGER tg_invoice_insert
BEFORE INSERT ON invoices
FOR EACH ROW
BEGIN
  INSERT INTO invoices_seq VALUES (NULL);
  SET NEW.invoice_number = CONCAT('LHPL', LPAD(LAST_INSERT_ID(), 3, '0'));
END");
	}
}
