<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct()
	{
		parent :: __construct();
		if(!$this->session->userdata('isLogin')){
			redirect( '/login' , 'refresh' );
		}
		$this->load->model('products_model');
		$this->load->model('general_model');
    }
	public function index()
	{	
		$headData['page_name']='products';
		$headData['custom_js']=array('product.js');
		$data['products']=$this->general_model->get_products();

		$this->load->view('inc/head',$headData);
		$this->load->view('prodcuct/list',$data);
		$this->load->view('inc/footer',$headData);
	}
	public function save()
	{
		$postData=$this->input->post();
		$results=$this->products_model->save($postData);
		echo json_encode($results);
	}
	public function lists()
	{
		$records=$this->products_model->lists();
		echo json_encode($records);
	}
	public function delete()
	{	
		$action_id=$this->input->post('action_id');
		$records=$this->products_model->delete($action_id);
		echo json_encode($records);
	}
	public function get()
	{
		$action_id=$this->input->post('action_id');

		if($action_id!=''){
			$results=$this->products_model->get($action_id);
			$data['postData']=$results['master_detail'];
			$data['details']=$results['details'];
		}

		$data['action_id']=$action_id;
		$data['mode']=$this->input->post('mode');
		$data['product_name']=$this->input->post('product_name');
		$data['taxes']=$this->general_model->get_taxes();
		$data['uoms']=$this->general_model->get_uom();
		$source=$this->load->view('prodcuct/add',$data,TRUE);
		echo $source;
	}
	public function get_batches()
	{	
		$action_id=$this->input->post('action_id');
		$records=$this->products_model->get_batches($action_id);
		echo json_encode($records);
	}
	public function get_product_detail()
	{
		$action_id=$this->input->post('action_id');
		$records=$this->products_model->get_product_detail($action_id);
		echo json_encode($records);
	}
	public function importproduct()
	{	//print_r($_FILES);
		$this->load->library('excel');
		$this->load->model('excel_model');

		$configUpload['upload_path'] = 'uploads/excel/';
     	$configUpload['allowed_types'] = 'xls|xlsx|csv';
     	$this->load->library('upload', $configUpload);
     	if ( ! $this->upload->do_upload('excelfile'))
		{
			$error = array('message' => $this->upload->display_errors(),"status"=>0);
			echo json_encode($error);
			exit();
		}
     	$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
     	$file_name = $upload_data['file_name']; //uploded file name
	 	$extension=$upload_data['file_ext'];    // uploded file extension
	 	
	 	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	 	//$objReader= PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007 	  
        //$objPHPExcel = PHPExcel_IOFactory::load("c:\cctv.xls");  // Remove the createReader line before this
        $objReader->setReadDataOnly(true); 		  
		$objPHPExcel=$objReader->load('uploads/excel/'.$file_name);		 
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0); 
		$excelData=array();
		for($i=2;$i<=$totalrows;$i++)
        {
              $product_name= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
              $price= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
              $uom= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
              $tax= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
              $description= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
              $qty= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
              $type= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
              $hsn_scn= $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
              $sku= $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
              $batch_no= $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();

              array_push($excelData, array("product_name"=>$product_name,"price"=>$price,"uom"=>$uom,"tax"=>$tax,"description"=>$description,
              							   "qty"=>$qty,"type"=>$type,"hsn_scn"=>$hsn_scn,"sku"=>$sku,"batch_no"=>$batch_no));
        }
        $returnArray=$this->excel_model->import_products($excelData);
        //print_r($returnArray);

       unlink('uploads/excel/'.$file_name);
	   $success = array('message' => 'Import process is in progress.',"status"=>1);
	   echo json_encode($success);
	}

	public function exportproduct()
	{
		$this->load->library('excel');
		$results=$this->products_model->get_all_product();
		$fields=$results['fields'];
		$rows=$results['rows'];

		$objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
 
        // Field names in the first row
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyle('1:1')->getFont()->setBold(true);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach($rows as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Products_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');
	}
}
