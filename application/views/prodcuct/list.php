<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<!-- <div class="page-head">
		<div class="container">
			<div class="page-title">
				<h1>Product Management <small>basic datatable samples</small></h1>
			</div>

			
		</div>
	</div> -->

	<div class="page-content">
		<div class="container">

			<div class="page-toolbar">
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="fab fa-searchengin font-green-sharp"></i>
									<span class="caption-subject font-green-sharp bold uppercase">Search Products</span>
								</div>
							</div>
							<div class="portlet-body form">
								<form action="javascript:;" class="form-horizontal product-serachfrm">
									<div class="form-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Product Name</label>
													<div class="col-md-6">
														<select class="form-control input-square product-custom-select2" name="product_id" placeholder=" Enter Product name">
														  <option value="">--Select Product--</option>
														  <?php 
														  	foreach ($products as $product) {
														  		echo '<option value="'.$product->product_id.'">'.$product->product_name.'</option>';
														  	}
														  ?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Report Type</label>
													<div class="col-md-6">
														<select class="form-control input-square" name="product_type">
														  <option>Items In Hand</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Unit Price Between</label>
													<div class="col-md-6 col2-row1">
														<input type="number" class="form-control input-square col-bw" name="min_price" placeholder=" Min">
														<span>and</span>
														<input type="number" class="form-control input-square col-bw" name="max_price" placeholder=" Max">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">SKU</label>
													<div class="col-md-6">
														<input type="text" class="form-control input-square" name="sku">
													</div>
												</div>
											</div>
										</div>	
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Quantity Between</label>
													<div class="col-md-6 col2-row1">
														<input type="number" class="form-control input-square col-bw" name="min_qty" placeholder=" Min">
														<span>and</span>
														<input type="number" class="form-control input-square col-bw" name="max_qty" placeholder=" Max">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-actions right">
													<button type="button" class="btn blue search-btn"><i class="fa fa-search"></i> Search</button>
													<button type="button" class="btn default">Reset</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="page-toolbar">
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="actions">
									<a href="javascript:;" class="btn btn-default btn-circle" onclick="add_product('','add')"><i class="fa fa-plus"></i><span class="hidden-480">New Product </span></a>
									<a href="javascript:;" class="btn btn-default btn-circle" data-toggle="modal" data-target="#import-modal"><i class="fa fa-upload"></i><span class="hidden-480"> Import </span></a>
									<a href="products/exportproduct" class="btn btn-default btn-circle"><i class="fa fa-download"></i><span class="hidden-480"> Export </span></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-container">
									<table class="table table-striped table-bordered table-hover" id="datatable_products">
										<thead>
											<tr>
												<th><i class="fa fa-hashtag"></i> SKU</th>
												<th> Name</th>
												<th> Description</th>
												<th> Quantity</th>
												<th> Price</th>
												<th> UoM</th>
												<th> Tax</th>
												<th class="no-sort">Actions</th>
											</tr>
										</thead>
										
									</table>
								</div>
							</div>		
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="target">
</div>

<div id="import-modal" class="modal fade" tabindex="-1" data-width="800">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Import Products</h4>
	</div>
	<div class="modal-body">
		<div class="import-process">
			<h3>Importing your products from Excel(.xls)</h3>
			<p class="follow-txt"><b>Follow this steps:</b></p>
			<ol>
				<li>Download the Excel template to your compurter by clicking the <b>Download Sample</b> button.</li>
				<li>Integrate your data into the template file, minding the column order .</li>
				<li>Click <b>Upload File.</b></li>
			</ol>
			<br><br>
			<p class="follow-txt"><b>Important:</b></p>
			<ul>
				<li>Do not change the column order in the template file.</li>
				<li>If you do not have data for a particular column, leave it empty and do not move or delete the column.</li>
			</ul>
		</div>
		
	</div>
	<div class="modal-footer">
		<!-- <form action="products/importproduct" method="post" enctype="multipart/form-data" >                     
		<label>Excel File:</label>                        
		<input type="file" name="excelfile" />				                   
		<input type="submit" value="upload" name="upload" />
		</form>	 -->
		<input type="file" class="import-file" name="excelfile">
		<a href="uploads/sample_files/product_sample.xls" download class="btn default dwnld-sample"><i class="fas fa-download"></i> Download Sample</a>
		<a href="#" class="btn default imprt-btn" onclick="upload_file()"><i class="fas fa-upload"></i> Upload File</a>
		
	</div>
</div>
