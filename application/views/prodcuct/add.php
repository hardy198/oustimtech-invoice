<?php 
if($mode=='add') { $header_title='Add Product';$btn_title='Save changes'; }
elseif($mode=='edit') { $header_title='Edit Product';$btn_title='Update changes'; }
elseif($mode=='view') { $header_title='View Product'; }

?>
<div id="saving-product-modal" class="modal fade" tabindex="-1" data-width="800">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"><?php echo $header_title; ?></h4>
	</div>
	<div class="modal-body">
		<form action="javascript:;" class="horizontal-form" id="product-save-frm">
			<input type="hidden" name="action_id" value="<?php echo $action_id; ?>" id="action_id">
			<div class="form-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Product name <span class="required">* </span></label>
							<input type="text" id="product_name" name="product_name" class="form-control" placeholder=" Enter product name" required value="<?php echo isset($postData) && $postData->product_name!='' ? $postData->product_name:$product_name; ?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Product type</label>
							<select class="form-control" name="product_type" id="product_type" >
								<option value="goods" <?php echo isset($postData) && $postData->product_type=='goods' ?'selected':''; ?>>Goods</option>
								<option value="service" <?php echo isset($postData) && $postData->product_type=='service' ?'selected':''; ?>>Service</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">SKU <span class="required">* </span></label>
							<input type="text" id="sku" name="sku" class="form-control" required value="<?php echo isset($postData) && $postData->sku!='' ? $postData->sku:''; ?>">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label"><span id="hsn_scn_label">HSN</span> <span class="required">* </span></label>
							<input type="text" id="hsn_scn" name="hsn_scn" class="form-control" required value="<?php echo isset($postData) && $postData->hsn_scn!='' ? $postData->hsn_scn:''; ?>">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">Tax <span class="required">* </span></label>
							<select class="form-control" name="tax_id" id="tax_id" required>
								<option value="">--Select Tax--</option>
								<?php foreach ($taxes as $tax) { ?>
									<option value="<?php echo $tax->tax_id; ?>" <?php echo isset($postData) && $postData->tax_id==$tax->tax_id ?'selected':''; ?>><?php echo $tax->tax_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">UoM</label>
							
							<select class="form-control" name="uom_id" id="uom_id" required>
								<option value="">--Select UOM--</option>
								<?php foreach ($uoms as $uom) { ?>
									<option value="<?php echo $uom->uom_id ?>" <?php echo isset($postData) && $postData->uom_id==$uom->uom_id ?'selected':''; ?>><?php echo $uom->unit_name.' ('.$uom->symbol.')'; ?></option>';
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Expiry date</label>
							<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
								<input type="text" name="expiry_date" class="form-control" id="date-mask" placeholder="dd-mm-yyyy" value="<?php echo isset($postData) && $postData->expiry_date!='' ? date('d-m-Y',strtotime($postData->expiry_date)):''; ?>">
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
							</div>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Product description</label>
							<textarea class="form-control" rows="3" name="product_desc" id="product_desc"><?php echo isset($postData) && $postData->product_desc!='' ? $postData->product_desc:''; ?></textarea>
						</div>
					</div>
				</div>
				<div class="batch-clone-target">
					<?php if($mode=='add'){ ?>
					<input type="hidden" name="product_detail_id[]" value="">
					<div class="row batch-row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Unit price <span class="required">* </span></label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-rupee-sign"></i></span>
									<input type="text" id="unit_price" name="price[]" class="form-control" required/>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Quantity <span class="required">* </span></label>
								<input type="number" id="qty" name="qty[]" class="form-control" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Batch No.</label>
								<input type="text" id="batch_no" name="batch_no[]" class="form-control" >
							</div>
						</div>
						<div class="col-md-3">
							<a href="javascript:;" class="btn btn-sm green add-batch"> <i class="fa fa-plus"></i></a>
						</div>
					</div>
					<?php } else{
						foreach ($details as $key => $value) { ?>
						<input type="hidden" name="product_detail_id[]" value="<?php echo $value->product_detail_id; ?>">
						<div class="row batch-row">
							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">Unit price <span class="required">* </span></label>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-rupee-sign"></i></span>
										<input type="text" id="unit_price" name="price[]" class="form-control" required value="<?php echo $value->price ?>"/>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label class="control-label">Quantity <span class="required">* </span></label>
									<input type="number" id="qty" name="qty[]" class="form-control" required value="<?php echo $value->batch_qty ?>">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label class="control-label">Remaining quantity <span class="required">* </span></label>
									<input type="number" id="remaining_qty" name="remaining_qty[]" class="form-control" required value="<?php echo $value->remaining_qty ?>">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label class="control-label">Batch No.</label>
									<input type="text" id="batch_no" name="batch_no[]" class="form-control" value="<?php echo $value->batch_no ?>">
								</div>
							</div>
							<?php if($mode!='view'){ ?>
							<div class="col-md-1">
								<?php if($key==0){ ?>
								<a href="javascript:;" class="btn btn-sm green add-batch"> <i class="fa fa-plus"></i></a>
								<?php } else { ?>
								<a href="javascript:;" class="btn btn-sm red remove-batch"> <i class="fa fa-minus"></i></a>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
						<?php } 
					} ?>
				</div>
			</div>
			<?php if($mode!='view') { ?>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
				<button type="submit" class="btn blue action-btn"><?php echo $btn_title; ?></button>
			</div>
			<?php } ?>
		</form>
	</div>
	
</div>

<div class="hidden batch-clone-src">
	<input type="hidden" name="product_detail_id[]" value="">
	<div class="row batch-row">
		<div class="col-md-3">
			<div class="form-group">
				<label class="control-label">Unit price <span class="required">* </span></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-rupee-sign"></i></span>
					<input type="text" id="unit_price" name="price[]" class="form-control" required/>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label class="control-label">Quantity <span class="required">* </span></label>
				<input type="number" id="qty" name="qty[]" class="form-control" required>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label class="control-label">Batch No.</label>
				<input type="text" id="batch_no" name="batch_no[]" class="form-control" >
			</div>
		</div>
		<div class="col-md-3">
			<a href="javascript:;" class="btn btn-sm red remove-batch"> <i class="fa fa-minus"></i></a>
		</div>
	</div>
</div>