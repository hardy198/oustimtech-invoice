<?php 
$items=$this->session->userdata('carts');
$shipping_charge=$this->session->userdata('shipping_charge');
$cart_discount=$this->session->userdata('cart_discount');

$subtotal=array();
$discount=array();
$cgst=array();
$sgst=array();
$igst=array();

if(count($items) > 0){
	foreach ($items as $key => $item) {
		$itemToatl=$item['price']*$item['qty'];
		if($item['discount']==''){ $item['discount']=0; }
		$itemDiscount=($itemToatl*$item['discount'])/100;

		$itemValueAfterDict=$itemToatl - $itemDiscount;
		$itemCGST=($itemValueAfterDict*$item['cgst'])/100;
		$itemSGST=($itemValueAfterDict*$item['sgst'])/100;
		$itemIGST=($itemValueAfterDict*$item['igst'])/100;

		array_push($subtotal,$itemToatl);
		array_push($discount,$itemDiscount);
		array_push($cgst,$itemCGST);array_push($sgst,$itemSGST);array_push($igst,$itemIGST);
	}
}
$subtotalValue=array_sum($subtotal);
$discountValue=array_sum($discount);
$cgstValue=array_sum($cgst);
$sgstValue=array_sum($sgst);
$igstValue=array_sum($igst);

$GSTValue=$cgstValue + $sgstValue;


if(isset($shipping_charge['shipping_cost']) && $shipping_charge['shipping_cost']!=0){
	$shipping_value=$shipping_charge['shipping_cost'];
	$shipping_tax_rate=$shipping_charge['shipping_tax_rate'];
	$shipping_cgst=$shipping_charge['shipping_cgst'];
	$shipping_sgst=$shipping_charge['shipping_sgst'];
	$shipping_igst=$shipping_charge['shipping_igst'];
	if($shipping_tax_rate!=0){
		$shipping_tax_CGST=($shipping_value*$shipping_cgst)/100;
		$shipping_tax_SGST=($shipping_value*$shipping_cgst)/100;
		$shipping_tax_IGST=($shipping_value*$shipping_igst)/100;
		$shipping_tax=$shipping_tax_CGST + $shipping_tax_SGST;
		$shipping_value= $shipping_value + $shipping_tax;
	}
	else{
		$shipping_tax_CGST=0;
		$shipping_tax_SGST=0;
		$shipping_tax_IGST=0;
		$shipping_tax=0;
		$shipping_value= 0.00;
	}
}else{
	$shipping_tax_CGST=0;
	$shipping_tax_SGST=0;
	$shipping_tax_IGST=0;
	$shipping_tax=0;
	$shipping_value= 0.00;
}
$shipping_charge['shipping_tax_CGST']=$shipping_tax_CGST;
$shipping_charge['shipping_tax_SGST']=$shipping_tax_SGST;
$shipping_charge['shipping_tax_IGST']=$shipping_tax_IGST;
$shipping_charge['shipping_tax']=$shipping_tax;
$shipping_charge['shipping_value']=number_format((float)$shipping_value, 2, '.', '');
$this->session->set_userdata("shipping_charge", $shipping_charge);

$cart_amounts['subtotal_discount']=$discountValue;
$cart_amounts['additional_discount']=0;
if(isset($cart_discount['discount_cost']) && $cart_discount['discount_cost']!=0){
	
	if(isset($cart_discount['discount_type']) && $cart_discount['discount_type']=='fixed')
	{
		$additionalDiscountValue=$cart_discount['discount_cost'];
	}
	if(isset($cart_discount['discount_type']) && $cart_discount['discount_type']=='percentage')
	{
		$additionalDiscountValue=($subtotalValue*$cart_discount['discount_cost'])/100;
	}
	$cart_amounts['additional_discount']=$additionalDiscountValue;
	$discountValue=$discountValue + $additionalDiscountValue;	
}

$cartTotal=$subtotalValue - $discountValue + $GSTValue + $shipping_value;
$roundedToatl = bcdiv($cartTotal, 1, 0);
$roundedOff= $cartTotal - $roundedToatl;
$roundedOff= bcdiv($roundedOff, 1, 2);

$cart_amounts['cart_total']=$roundedToatl;
$cart_amounts['subtotal']=$subtotalValue;
$cart_amounts['total_discount']=$discountValue;
$cart_amounts['total_taxable_value']=$subtotalValue - $discountValue;
$cart_amounts['rounded_off']=$roundedOff;
$this->session->set_userdata("cart_amounts", $cart_amounts);

?>
<ul class="list-unstyled amounts">
			<li>
				<strong>Sub - Total amount:</strong> 
				<i class="fas fa-rupee-sign"></i><label class="sub-total"><?php echo $subtotalValue; ?></label>
			</li>
			<li>
				<strong>Discount:</strong> 
				(-)<i class="fas fa-rupee-sign"></i><label class="total-discount"> <?php echo $discountValue; ?></label>
			</li>
			<li>
				<strong>CGST rate:</strong>
				<i class="fas fa-rupee-sign"></i><label class="gst-rate"> <?php echo $cgstValue; ?></label>
			</li>
			<li>
				<strong>SGST rate:</strong>
				<i class="fas fa-rupee-sign"></i><label class="gst-rate"> <?php echo $sgstValue; ?></label>
			</li>
			<li class="shipping-total" <?php echo isset($shipping_value) && $shipping_value!='0.00'? 'style="display:block"':'style="display:none"' ?>>
				<strong>Shipping & packaging:</strong>
				<i class="fas fa-rupee-sign"></i><label class="shipping-packaging-charge"> <?php echo $shipping_value; ?></label>
			</li>
			<li>
				<strong>Rounded Off:</strong>
				(-)<i class="fas fa-rupee-sign"></i><label class="rounded-off-value"> <?php echo $roundedOff; ?></label>
			</li>
			<hr>
			<li>
				<strong>Total:</strong>
				<i class="fas fa-rupee-sign"></i><label class="cart-total"> <?php echo $roundedToatl; ?></label>
			</li>
</ul>