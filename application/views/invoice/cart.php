<?php 
$items=$this->session->userdata('carts');
$cart_discount=$this->session->userdata('cart_discount');
?>
<div class="row">
	<div class="col-md-12">
		<div class="table-scrollable">
			<table class="table table-striped table-bordered table-advance table-hover" id="invoice_products">
				<thead>
					<tr>
						<th>#</th>
						<th>product/Service</th>
						<th>HSN/SAC</th>
						<th>Description</th>
						<th>UoM</th>
						<th>Qty</th>
						<th>Price</th>
						<th>Discount</th>
						<th>Value</th>
						<th>CGST</th>
						<th>SGST</th>
						<th>IGST</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(count($items) > 0) {
					$cart_no=1; foreach ($items as $key => $value) { 
						
						$product_value=$value['price']*$value['qty'];
						$discount_value=($product_value*$value['discount'])/100;
    					$discount_value=number_format((float)$discount_value, 2, '.', '');
    					$product_value_after_discount=$product_value - $discount_value;

    					$cgst_value=($product_value_after_discount*$value['cgst'])/100;
    					$sgst_value=($product_value_after_discount*$value['sgst'])/100;
						
						$items[$key]['taxable_value']=$product_value_after_discount;
    					$items[$key]['discount_value']= $discount_value;
    					$items[$key]['cgst_value']=$cgst_value;
    					$items[$key]['sgst_value']=$sgst_value;
    					$items[$key]['tax_amount']=round($cgst_value + $sgst_value,2);
    					$items[$key]['item_subtotal']=$product_value+$cgst_value+$sgst_value-$discount_value;
					?>
					<tr>
						<td><?php echo $cart_no; ?></td>
						<td><?php echo $value['product_name']; ?></td>
						<td><?php echo $value['hsn_scn']; ?></td>
						<td><?php echo $value['desc']; ?></td>
						<td><?php echo $value['uom']; ?></td>
						<td><?php echo $value['qty']; ?></td>
						<td><?php echo $value['price']; ?></td>
						<td><?php echo isset($value['discount']) && $value['discount']!='' ? $value['discount']:0; ?>%</td>
						<td><?php echo $product_value_after_discount; ?></td>
						<td><?php echo $cgst_value; ?></td>
						<td><?php echo $sgst_value; ?></td>
						<td><?php echo '0'; ?></td>
						<td><a href="javascript:;" class="blue-btn" onclick="edit_cart('<?php echo $key; ?>')"><i class="fa fa-edit"></i> Edit </a>&nbsp;&nbsp;<a href="javascript:;" onclick="remove_cart_item('<?php echo $key; ?>',this)" class="red-btn"><i class="fa fa-trash-alt"></i> delete </a></td>
					</tr>
					<?php $cart_no++; } $this->session->set_userdata("carts", $items); } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>