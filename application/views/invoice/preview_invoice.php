<?php 
$items=$this->session->userdata('carts');
$shipping_charge=$this->session->userdata('shipping_charge');
$cart_discount=$this->session->userdata('cart_discount');
$customer_details=$this->session->userdata('customer_details');
$cart_amounts=$this->session->userdata('cart_amounts');

print_r($items);
$CGST_TOTAL=0.00;
$SGST_TOTAL=0.00;
$IGST_TOTAL=0.00;
$AMOUNT_TOTAL=0.00;
?>
<style type="text/css">
	@import url("https://fonts.googleapis.com/css?family=Martel+Sans:400,600,800|Open+Sans:400,600,800&subset=devanagari");
	html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
    margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;}
	.preview-container{width: 990px;margin: 0 auto;}
	.page {width: 100%;table-layout: fixed;font-family: 'Open Sans', 'Martel Sans', sans-serif;color: #7a858f;line-height: 22px;}
	.page td {width: 100%;}
	.document-details {position: relative;}
	.document-details .document-label {text-align: right;font-size: 14px;line-height: 24px;font-weight: 600;}
	.data {float: right;margin-bottom: 20px;color: #3a3030;text-align: right;width: 50%;}
	.data .data__identifier {margin-bottom: 25px;padding-left: 15px;font-size: 28px;line-height: 32px;color: #28354d;font-weight: 600;text-transform: uppercase;}
	.data .data__identifier .invoice-label {color: #28354d;}
	.data .invoice-label {float: left;}
	.data .data__amount-due {background-color: #2a6282 !important;border: 1px solid #2a6282;color: #fff;padding: 0 15px;font-size: 14px;line-height: 30px;margin-bottom: 15px;padding-left: 15px;}
	.data .data__amount-due .invoice-label {color: #fff;}
	.organization {width: 48%;padding-right: 10px;float: left;margin-bottom: 20px;color: #3a3030;}
	.organization .organization__name {font-size: 28px;line-height: 32px;margin-bottom: 34px;color: #06263a;font-weight: 800;}
	.cliven {clear: both;width: 100%;margin-bottom: 20px;color: #3a3030;}
	.cliven .cliven__billing {float: left;width: 46%;padding-right: 10px;}
	.cliven .cliven__shipping {float: right;width: 49%;padding-left: 15px;}
	.cliven h3 {font-size: 13px;font-weight: 600;color: #06263a;margin: 0;}
	.cliven .cliven__name {font-size: 14px;font-weight: 600;color: #06263a;}

	table {border-collapse: collapse;border-spacing: 0;}
	.line-items{display: block;margin-top: 20px;}
	.line-items table {table-layout: fixed;border-collapse: separate;width: 100%;color: #3a3030;}
	.line-items th, .line-items td {text-align: right;font-size: 13px;}
	.line-items thead {display: table-header-group;}
	
	.line-items th span,.line-items td span{display: block;font-size: 11px;}
	.line-items thead th {padding: 5px 3px;line-height: 18px;background-color: #2a6282 !important;border-top: 1px solid #2a6282;
		border-right: 1px solid #2a6282;border-bottom: 1px solid #2a6282;color: #fff;font-weight: 600;
    	white-space: nowrap;text-overflow: ellipsis;text-overflow: ellipsis-word;text-overflow: clip;overflow: hidden;vertical-align: middle;}
   
    .line-items thead th:first-child {border-left: 1px solid #2a6282;}
    .line-items thead th:last-child {padding-right: 7px;}
    .line-items tbody tr:first-child td {padding-top: 10px;}
    .line-items tbody td {font-size: 12px;line-height: 20px;padding: 1px 3px;}
    .line-items tbody td {border-right: 1px solid #e3e6ea;font-weight: 600;}
   	
   	.line-items .line-item-row__index{font-size: 11px;width: 30px;text-align: center;}
   	/*.line-items .line-item-row__qty{width: 50px;}
   	.line-items .line-item-row__code{width: 62px;}
   	.line-items .line-item-row__price{width: 95px;}
   	.line-items .line-item-row__discount,.line-items .line-item-row__tax{width: 85px;}
   	.line-items .line-item-row__amount{width: 100px;}*/
   	.line-items .line-item-row__name{text-align: left;} 
    .line-items tbody td.line-item-row__name h3 {font-size: 13px;line-height: 16px;margin-bottom: 3px;color: #266a95;}
    .line-items tbody td.line-item-row__name p {font-size: 10px;line-height: 14px;}
    .line-items tbody td span{font-size: 10px;color: #5e5e5e;line-height: 14px;}

    .line-items tbody tr.shipping-rate-row td {padding-top: 5px;padding-bottom: 5px;line-height: 30px;}
    .line-items tbody tr:last-child td {border-bottom: 1px solid #e3e6ea;}
    .line-items tr.shipping-rate-row td.invoice-label {padding-right: 52px;}
    .summary>td {padding-bottom: 20px;}
    .summary .amounts {width: 479px;float: right;color: #06263a;}
    .summary .amounts .tax-summaries {margin-bottom: 15px;table-layout: fixed;border-collapse: separate;width: 100%;}
    .summary .amounts .tax-summaries tr:last-child td:first-child, .summary .amounts .tax-summaries tr:last-child td.tax-row__percent {border-bottom: none;}
    .summary .amounts .tax-summaries td {font-size: 12px;line-height: 18px;padding: 5px 3px;}
    .summary .amounts .tax-summaries td.invoice-label {text-align: right;border-right: none;}
    .summary .amounts .tax-summaries td:first-child {background-color: #fff;}
    .summary .amounts .tax-summaries td {text-align: right;border-right: 1px solid #e3e6ea;font-weight: 600;background-color: #fafafa;}
    .summary .amounts .tax-summaries tr:last-child td {border-bottom: 1px solid #e3e6ea;}
    .summary .amounts .amounts_row{font-size: 13px;}
    .summary .amounts .amounts_row span.invoice-label {width: 195px;color: #06263a;float: left;font-weight: 600;}
    .summary .amounts .amounts_row span {padding: 1px 7px 1px 3px;display: block;text-align: right;}
    .summary .amounts .amounts_row span.amount {width: calc(100% - 215px);float: right;font-weight: 600;}
    .document-terms-signatures td {padding: 20px 0 15px 0;color: #3a3030;border-top: 1px solid #e3e6ea;}
    .document-terms-signatures.one-signature .signatures {width: 25%;float: right;margin-left: 20px;}
    .document-terms-signatures.one-signature .signatures .signature {width: 100%;float: right;}
    .document-terms-signatures .signatures .signature h4 {border-top: 1px solid #e3e6ea;padding-top: 10px;padding-left: 3px;margin-top: 65px;color: #072d45;font-weight: 600;margin-bottom: 10px;font-size: 13px;}
</style>
<div class="preview-container">
<table class="page">
	<thead>
		<tr>
			<td>
				<div class="document-details ">
                  <div class="document-label">Original Copy</div>
                </div>

                <div class="data">
                	<div class="data__identifier row"><span class="invoice-label">Tax Invoice</span> #1</div>
                	<div class="data__amount-due row"><span class="invoice-label">Amount Due:</span>₹<?php echo $cart_amounts['cart_total']; ?></div>
                </div>	

                <div class="organization">
				  	<div class="organization__name"><?php echo $customer_details['customer_name']; ?></div>
				  	<div><?php echo $customer_details['phone_number']; ?></div>
				  	<div><?php echo $customer_details['email_address']; ?></div>
				  	<div class="organization__fiscal-identifiers">
				    	<span><strong>GSTIN:</strong> <?php echo $customer_details['gstin']; ?></span>
				  	</div>
				</div>

				<div class="cliven">
					<div class="cliven__billing">
				      <h3>Bill To</h3>
				      <div class="cliven__name"><?php echo $customer_details['customer_name']; ?></div>
					  <div class="cliven__address"><?php echo $customer_details['billing_address']; ?></div>
				    </div>
				    <div class="cliven__shipping">
				        <h3>Ship To</h3>
				        <div class="cliven__name"><?php echo $customer_details['customer_name']; ?></div>
						<div class="cliven__address"><?php echo $customer_details['shipping_address']; ?></div>
				    </div>
				</div>

			</td>
		</tr>
	</thead>

	<tbody>
		<tr class="line-items">
			<td>
				<table>
					<thead>
						<tr>
							<th class="line-item-row__index">S.No</th>
							<th class="line-item-row__name">Item<br>Description</th>
							<th class="line-item-row__code">HSN/SAC</th>
							<th class="line-item-row__qty">Qty<span class="uom">UoM</span></th>
							<th class="line-item-row__price">Price <span class="currency">(₹)</span></th>
							<th class="line-item-row__discount">Discount <span class="currency">(₹)</span></th>
							<th class="line-item-row__tax">Taxable Value <span class="currency">(₹)</span></th>
							<th class="line-item-row__tax">CGST <span class="currency">(₹)</span></th>
							<th class="line-item-row__tax">SGST <span class="currency">(₹)</span></th>
							<th class="line-item-row__tax">IGST <span class="currency">(₹)</span></th>
							<th class="line-item-row__amount">Amount <span class="currency">(₹)</span></th>
						</tr>
					</thead>
					<tbody>
						<?php $s_no=1; foreach ($items as $item) { ?>
							<tr>
								<td class="line-item-row__index"><?php echo $s_no; ?></td>
								<td class="line-item-row__name">
									<h3><?php echo $item['product_name']; ?></h3>
									<p><?php echo $item['desc']; ?></p>
								</td>
								<td class="line-item-row__code"><?php echo $item['hsn_scn']; ?></td>
								<td class="line-item-row__qty"><?php echo $item['qty']; ?>
									<span class="uom"><?php echo $item['uom']; ?></span>
								</td>
								<td class="line-item-row__price"><?php echo $item['price']; ?></td>
								<td class="line-item-row__discount"><?php echo $item['discount_value']; ?>
									<span class="percentage"><?php echo isset($value['discount_value']) && $value['discount_value']!='' ? $value['discount_value']:0; ?>%</span>
								</td>
								<td class="line-item-row__tax"><?php echo $item['taxable_value']; ?></td>
								<td class="line-item-row__tax"><?php echo $item['cgst_value']; ?>
									<span class="percentage"><?php echo $item['cgst']; ?>%</span>
								</td>
								<td class="line-item-row__tax"><?php echo $item['sgst_value']; ?>
									<span class="percentage"><?php echo $item['sgst']; ?>%</span>
								</td>
								<td class="line-item-row__tax">0.00
									<span class="percentage">0%</span>
								</td>
								<td class="line-item-row__amount">
									<?php echo $item['item_subtotal']; ?>
								</td>
							</tr>
						<?php $s_no++; $CGST_TOTAL+=$item['cgst_value']; $SGST_TOTAL+=$item['sgst_value']; $AMOUNT_TOTAL+=$item['item_subtotal']; } ?>
							<?php if(isset($shipping_charge['shipping_cost']) && $shipping_charge['shipping_cost']!='' && $shipping_charge['shipping_cost']!=0){ ?>
							<tr class="shipping-rate-row">
							  	<td colspan="6" class="invoice-label">Shipping &amp; Packaging charges</td>
							  	<td class="line-item-row__tax">
							  		<?php echo $shipping_charge['shipping_cost']; ?>
							  	</td>
							  	<td class="line-item-row__tax">
							  		<?php echo $shipping_charge['shipping_tax_CGST']; ?>
							  		<span class="percentage"><?php echo isset($shipping_charge['shipping_cgst']) && $shipping_charge['shipping_cgst']!='' ? $shipping_charge['shipping_cgst']:0; ?>%</span>
							  	</td>
							  	<td class="line-item-row__tax">
							  		<?php echo $shipping_charge['shipping_tax_SGST']; ?>
							  		<span class="percentage"><?php echo isset($shipping_charge['shipping_sgst']) && $shipping_charge['shipping_sgst']!='' ? $shipping_charge['shipping_sgst']:0; ?>%</span>
							  	</td>
							  	<td class="line-item-row__tax">
							  		0.00
							  		<span class="percentage"><?php echo isset($shipping_charge['shipping_igst']) && $shipping_charge['shipping_igst']!='' ? $shipping_charge['shipping_igst']:0; ?>%</span>
							  	</td>
							  	<td class="line-item-row__amount">
							  		<?php echo $shipping_charge['shipping_value']; ?>
							  	</td>
							</tr>
							<?php $CGST_TOTAL+=$shipping_charge['shipping_tax_CGST']; $SGST_TOTAL+=$shipping_charge['shipping_tax_SGST']; $AMOUNT_TOTAL+=$shipping_charge['shipping_value']; } ?>
					</tbody>
				</table>
			</td>
		</tr>

		<tr class="summary extendable-amounts">
			<td>
				<div class="amounts">
					<table class="tax-summaries">
	                  	<tbody>
	                      <tr>
	                        <td class="invoice-label">Total&nbsp;&nbsp;&nbsp;</td>
	                        <td class="tax-row__tax"><?php echo $CGST_TOTAL; ?></td>
	                        <td class="tax-row__tax"><?php echo $SGST_TOTAL; ?></td>
	                        <td class="tax-row__tax">0.00</td>
	                        <td class="tax-row__amount"><?php echo $AMOUNT_TOTAL; ?></td>
	                      </tr>
	                  	</tbody>
	                </table>

	                <div class="amounts_row amounts__discount">
                      <span class="invoice-label">Discount</span>
                      <span class="amount">(-) ₹<?php echo $cart_amounts['total_discount']; ?></span>
                    </div>

                    <div class="amounts_row amounts__taxable">
	                    <span class="invoice-label">Total taxable value</span>
	                    <span class="amount">₹<?php echo $cart_amounts['total_taxable_value']; ?></span>
	                </div>

	                <div class="amounts_row amounts__rounding-amount">
                      <span class="invoice-label">Rounded Off</span>
                      <span class="amount">(-)₹<?php echo $cart_amounts['rounded_off']; ?></span>
                    </div>

                    <div class="amounts_row amounts__total">
	                    <span class="invoice-label">Total <em>Invoice</em> Value (in figure)</span>
	                    <span class="amount">₹<?php echo $cart_amounts['cart_total']; ?></span>
	                </div>

                  	<div class="amounts_row amount-in-words">
                      <span class="invoice-label">Total <em>Invoice</em> Value (in words)</span>
                      <span class="amount">₹ <?php echo number_to_word($cart_amounts['cart_total']); ?></span>
                    </div>

				</div>
			</td>
		</tr>

		<tr class="document-terms-signatures one-signature">
            <td>
                <div class="signatures">
    				<div class="signature primary"><h4>Provider Signature</h4></div>
				</div>
            </td>
        </tr>
	</tbody>

</table>
</div>

<?php 
function number_to_word( $num = '' )
{
    $num    = ( string ) ( ( int ) $num );
   
    if( ( int ) ( $num ) && ctype_digit( $num ) )
    {
        $words  = array( );
       
        $num    = str_replace( array( ',' , ' ' ) , '' , trim( $num ) );
       
        $list1  = array('','one','two','three','four','five','six','seven',
            'eight','nine','ten','eleven','twelve','thirteen','fourteen',
            'fifteen','sixteen','seventeen','eighteen','nineteen');
       
        $list2  = array('','ten','twenty','thirty','forty','fifty','sixty',
            'seventy','eighty','ninety','hundred');
       
        $list3  = array('','thousand','million','billion','trillion',
            'quadrillion','quintillion','sextillion','septillion',
            'octillion','nonillion','decillion','undecillion',
            'duodecillion','tredecillion','quattuordecillion',
            'quindecillion','sexdecillion','septendecillion',
            'octodecillion','novemdecillion','vigintillion');
       
        $num_length = strlen( $num );
        $levels = ( int ) ( ( $num_length + 2 ) / 3 );
        $max_length = $levels * 3;
        $num    = substr( '00'.$num , -$max_length );
        $num_levels = str_split( $num , 3 );
       
        foreach( $num_levels as $num_part )
        {
            $levels--;
            $hundreds   = ( int ) ( $num_part / 100 );
            $hundreds   = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '' );
            $tens       = ( int ) ( $num_part % 100 );
            $singles    = '';
           
            if( $tens < 20 )
            {
                $tens   = ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
            }
            else
            {
                $tens   = ( int ) ( $tens / 10 );
                $tens   = ' ' . $list2[$tens] . ' ';
                $singles    = ( int ) ( $num_part % 10 );
                $singles    = ' ' . $list1[$singles] . ' ';
            }
            $words[]    = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        }
       
        $commas = count( $words );
       
        if( $commas > 1 )
        {
            $commas = $commas - 1;
        }
       
        $words  = implode( ', ' , $words );
       
        //Some Finishing Touch
        //Replacing multiples of spaces with one space
        $words  = trim( str_replace( ' ,' , ',' , trim_all( ucwords( $words ) ) ) , ', ' );
        if( $commas )
        {
            $words  = str_replace_last( ',' , ' and' , $words );
        }
       
        return $words;
    }
    else if( ! ( ( int ) $num ) )
    {
        return 'Zero';
    }
    return '';
}
function trim_all( $str , $what = NULL , $with = ' ' )
{
    if( $what === NULL )
    {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space
       
        $what   = "\\x00-\\x20";    //all white-spaces and control chars
    }
   
    return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
}
function str_replace_last( $search , $replace , $str ) {
    if( ( $pos = strrpos( $str , $search ) ) !== false ) {
        $search_length  = strlen( $search );
        $str    = substr_replace( $str , $replace , $pos , $search_length );
    }
    return $str;
}
?>