<?php 
if($mode=='add') { $header_title='Add Invoice';$btn_title='Save changes'; }
elseif($mode=='edit') { $header_title='Edit Invoice';$btn_title='Update changes'; }
elseif($mode=='view') { $header_title='View Invoice'; }
?>
<div id="saving-modal" class="modal fade" tabindex="-1" data-width="1220">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"><?php echo $header_title; ?></h4>
	</div>
	<div class="modal-body">
		<form action="javascript:;" class="horizontal-form" id="invoice-save-frm">
			<div class="form-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group full-width">
							<label class="col-md-4 control-label">Client</label>
							<div class="col-md-8">
								<select class="form-control input-square customer-custom-select2" name="customer_id" placeholder=" Enter Client name" onchange="get_shipping_address(this)" required>
								  <option value="">--Select Client--</option>
								  <?php 
								  	foreach ($customers as $customer) {
								  		echo '<option value="'.$customer->customer_id.'">'.$customer->customer_name.'</option>';
								  	}
								  ?>
								</select>
							</div>
						</div>
						<div class="form-group full-width height80 shipTo">

						</div>

						<div class="form-group full-width">
							<label class="col-md-4 control-label">Place of supply</label>
							<div class="col-md-8">
								<select class="form-control input-square select2" name="city_id">
								  <option value="1">Gujarat</option>
								</select>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group full-width">
							<label class="col-md-5 control-label">Document Number</label>
							<div class="col-md-7">
								<input type="text" class="form-control input-square" name="document_number">
							</div>
						</div>
						<div class="form-group full-width">
							<label class="col-md-5 control-label">P.O Number</label>
							<div class="col-md-7">
								<input type="text" class="form-control input-square" name="po_number">
							</div>
						</div>
						<div class="form-group full-width">
							<label class="col-md-5 control-label">P.O Date</label>
							<div class="col-md-7">
								<input class="form-control form-control-inline date-picker" name="order_date" size="16" type="text" value=""/>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group full-width">
							<label class="col-md-5 control-label">Issue Date</label>
							<div class="col-md-7">
								<input class="form-control form-control-inline date-picker" name="issue_date" size="16" type="text" value=""/>
							</div>
						</div>
						<div class="form-group full-width">
							<label class="col-md-5 control-label">Payment Terms</label>
							<div class="col-md-7">
								<select class="form-control input-square" name="invoice_type_id">
								  <option value="1">Due on the specified Date</option>
								</select>
							</div>
						</div>
						<div class="form-group full-width">
							<label class="col-md-5 control-label">Due Date</label>
							<div class="col-md-7">
								<input class="form-control form-control-inline date-picker" name="due_date" size="16" type="text" value=""/>
							</div>
						</div>
					</div>

				</div>
				<hr>
				<input type="hidden" name="hidden_product_hsn_sac">
				<input type="hidden" name="rowId">

				<div class="row invoice_add_product_frm">
					<div class="col-md-2 col-pad pad-left15">
						<div class="form-group">
							<label class="control-label">Product</label>
							<select class="form-control input-square product-custom-select2" name="product_id" id="product_id">
							  <option value="">--Select Product--</option>
							  <?php 
							  	foreach ($products as $product) {
							  		echo '<option value="'.$product->product_id.'">'.$product->product_name.'</option>';
							  	}
							  ?>
							</select>
						</div>
					</div>

					<div class="col-md-1 col-pad">
						<div class="form-group">
							<label class="control-label">Batch No.</label>
							<select class="form-control input-square default-select2" name="batch_no" id="invoice_batch_no">
							  <option value=""></option>
							  
							</select>
						</div>
					</div>

					<div class="col-md-3 col-pad">
						<div class="form-group">
							<label class="control-label">Description</label>
							<input type="text" class="form-control input-square" name="invoice_description">
						</div>
					</div>

					<div class="col-md-1 col-pad">
						<div class="form-group">
							<label class="control-label">UoM</label>
							<select class="form-control" name="uom_id" id="uom_id">
								<option value=""></option>
								<?php foreach ($uoms as $uom) { ?>
									<option value="<?php echo $uom->uom_id ?>" <?php echo isset($postData) && $postData->uom_id==$uom->uom_id ?'selected':''; ?>><?php echo $uom->unit_name.' ('.$uom->symbol.')'; ?></option>';
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="col-md-1 col-pad">
						<div class="form-group">
							<label class="control-label">Qty</label>
							<input type="number" class="form-control input-square" name="qty">
						</div>
					</div>

					<div class="col-md-1 col-pad">
						<div class="form-group">
							<label class="control-label">Price</label>
							<input type="text" class="form-control input-square" name="price" id="price">
						</div>
					</div>

					<div class="col-md-1 col-pad">
						<div class="form-group">
							<label class="control-label">Discount (%)</label>
							<input type="text" class="form-control input-square" name="discount">
						</div>
					</div>

					<div class="col-md-1 col-pad">
						<div class="form-group">
							<label class="control-label">Tax</label>
							<select class="form-control" name="tax_id" id="tax_id">
								<option value=""></option>
								<?php foreach ($taxes as $tax) { ?>
									<option value="<?php echo $tax->tax_id; ?>" <?php echo isset($postData) && $postData->tax_id==$tax->tax_id ?'selected':''; ?>><?php echo $tax->tax_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="col-md-1 col-pad pad-right15">
						<div class="form-group">
							<a href="javascript:;" class="btn btn-sm green add-batch add-to-cart" onclick="add_products_to_list()"> <i class="fa fa-plus"></i> <label>ADD</label></a>
						</div>
					</div>
						
				</div>		

				<div class="invoice_products_target">
					<?php $this->load->view('invoice/cart'); ?>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="invoice-xtra-sec">
							<div class="xtra-row">
								<div class="col-md-6">
									<label>
										<input type="checkbox" class="icheck shipping-tick" data-checkbox="icheckbox_square-green"> Add shipping & packging costs 
									</label>
								</div>
								<div class="shipping-options col-md-6">
									<div class="col">
										<input type="number" class="form-control input-square shipping-cost" name="shipping_cost">
										<i class="fa fa-rupee-sign"></i>
									</div>
									<div class="col">
										<span>Tax</span>
										<select class="form-control" name="shipping_tax" id="shipping_tax">
											<option value=""></option>
											<?php foreach ($taxes as $tax) { ?>
												<option value="<?php echo $tax->tax_id; ?>" <?php echo isset($postData) && $postData->tax_id==$tax->tax_id ?'selected':''; ?>><?php echo $tax->tax_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="xtra-row">
								<div class="col-md-6">
									<label>
										<input type="checkbox" class="icheck discount-tick" data-checkbox="icheckbox_square-green"> Add discount to all
									</label>
								</div>
								<div class="discount-options col-md-6">
									<div class="col">
										<input type="number" class="form-control input-square discount_all_value" name="discount_all_value">
									</div>
									<div class="col">
										<select class="form-control" name="discount_type" id="discount_type">
											<option value="percentage">%</option>
											<option value="fixed">Fixed</option>
										</select>
									</div>
								</div>
							</div>
							<div class="xtra-row">
								<div class="col-md-12">
									<label>
										<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-green"> Subject to reverce charges 
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 invoice-block cart-total-sec">
						<?php $this->load->view('invoice/cart_total'); ?>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<label>Note for client</label>
						<textarea class="form-control" rows="3" name="client_notes" placeholder="ex: Declaration or Terms & Conditions"></textarea>
					</div>
					<div class="col-md-4">
						<label>Private notes (not shown to client)</label>
						<textarea class="form-control" rows="3" name="private_notes"></textarea>
					</div>
					<div class="invoice-action">
						<a href="#" class="btn btn-primary preview-content">Preview content</a>
						<a href="javascript:;" onclick="save_invoice()" class="btn btn-success save-invoice">Save invoice</a>
					</div>
				<div>
			</div>
		</form>
	</div>
	
</div>
