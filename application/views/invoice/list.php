<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<!-- <div class="page-head">
		<div class="container">
			<div class="page-title">
				<h1>Product Management <small>basic datatable samples</small></h1>
			</div>

			
		</div>
	</div> -->

	<div class="page-content">
		<div class="container">

			<div class="page-toolbar">
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="fab fa-searchengin font-green-sharp"></i>
									<span class="caption-subject font-green-sharp bold uppercase">Search Invoices</span>
								</div>
							</div>
							<div class="portlet-body form">
								<form action="javascript:;" class="form-horizontal product-serachfrm">
									<div class="form-body">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-4 control-label">Client Name</label>
													<div class="col-md-8">
														<select class="form-control input-square custom-select2" name="customer_id" placeholder=" Enter Client name">
														  <option value="">--Select Client--</option>
														  <?php 
														  	foreach ($products as $product) {
														  		echo '<option value="'.$product->product_id.'">'.$product->product_name.'</option>';
														  	}
														  ?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="col-md-4 control-label">Status</label>
													<div class="col-md-8">
														<select class="form-control input-square" name="status_id">
														  <option value="1">All</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<label class="col-md-4 control-label">Issued Between</label>
													<div class="col-md-6">
														<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
															<input type="text" class="form-control" name="from">
															<span class="input-group-addon">
															to </span>
															<input type="text" class="form-control" name="to">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-4 control-label">Invoice Number</label>
													<div class="col-md-8">
														<input type="text" class="form-control input-square" name="invoice_number">
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="col-md-4 control-label">Type</label>
													<div class="col-md-8">
														<select class="form-control input-square" name="invoice_type_id">
														  <option value="1">All</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<label class="col-md-4 control-label">Due Between</label>
													<div class="col-md-6">
														<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
															<input type="text" class="form-control" name="from">
															<span class="input-group-addon">
															to </span>
															<input type="text" class="form-control" name="to">
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-4 control-label">City</label>
													<div class="col-md-8">
														<select class="form-control input-square select2" name="city_id">
														  <option value="1">All</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="col-md-4 control-label">Quick Search</label>
													<div class="col-md-8">
														<input type="text" class="form-control input-square" name="invoice_number">
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="form-actions right">
													<button type="button" class="btn blue search-btn"><i class="fa fa-search"></i> Search</button>
													<button type="button" class="btn default">Reset</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="page-toolbar">
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="actions">
									<a href="javascript:;" class="btn btn-default btn-circle" onclick="open_popup('','add')"><i class="fa fa-plus"></i><span class="hidden-480">New Invoice </span></a>
									<a href="products/exportproduct" class="btn btn-default btn-circle"><i class="fa fa-download"></i><span class="hidden-480"> Export </span></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-container">
									<table class="table table-striped table-bordered table-hover" id="datatable_products">
										<thead>
											<tr>
												<th><i class="fa fa-hashtag"></i> SKU</th>
												<th> Name</th>
												<th> Description</th>
												<th> Quantity</th>
												<th> Price</th>
												<th> UoM</th>
												<th> Tax</th>
												<th class="no-sort">Actions</th>
											</tr>
										</thead>
										
									</table>
								</div>
							</div>		
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="target">
</div>

<div id="invoice-preview-modal" class="modal fade" tabindex="-1" data-width="1220">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<a href="#" class="btn btn-md default" onclick="generate_pdf()"><i class="far fa-file-pdf"></i> PDF</a>
		<a href="<?php echo base_url(); ?>/invoices/generate_pdf"  class="btn btn-md default"><i class="fas fa-print"></i> Print</a>
		<a href="javascript:;" onclick="save_invoice()" class="btn btn-md default"><i class="far fa-save"></i> Save Invoice</a>
	</div>
	<div class="modal-body">
		<div class="preview-invoice-body"></div>
	</div>
</div>


<div id="import-modal" class="modal fade" tabindex="-1" data-width="800">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Import Products</h4>
	</div>
	<div class="modal-body">
		<div class="import-process">
			<h3>Importing your products from Excel(.xls)</h3>
			<p class="follow-txt"><b>Follow this steps:</b></p>
			<ol>
				<li>Download the Excel template to your compurter by clicking the <b>Download Sample</b> button.</li>
				<li>Integrate your data into the template file, minding the column order .</li>
				<li>Click <b>Upload File.</b></li>
			</ol>
			<br><br>
			<p class="follow-txt"><b>Important:</b></p>
			<ul>
				<li>Do not change the column order in the template file.</li>
				<li>If you do not have data for a particular column, leave it empty and do not move or delete the column.</li>
			</ul>
		</div>
		
	</div>
	<div class="modal-footer">
		<!-- <form action="products/importproduct" method="post" enctype="multipart/form-data" >                     
		<label>Excel File:</label>                        
		<input type="file" name="excelfile" />				                   
		<input type="submit" value="upload" name="upload" />
		</form>	 -->
		<input type="file" class="import-file" name="excelfile">
		<a href="uploads/sample_files/product_sample.xls" download class="btn default dwnld-sample"><i class="fas fa-download"></i> Download Sample</a>
		<a href="#" class="btn default imprt-btn" onclick="upload_file()"><i class="fas fa-upload"></i> Upload File</a>
		
	</div>
</div>
