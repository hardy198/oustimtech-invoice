<?php 
class general_model extends CI_Model 
{   
    public function __construct()
    {   parent :: __construct();
        $this->company_id = $this->session->userdata('company_id');
    }
	public function get_taxes()
    {
        $query = $this->db->get('taxes');
        return $query->result();
    }
    public function get_uom()
    {	
    	$this->db->where('status',1);
    	$query = $this->db->get('uom');
        return $query->result();
    }
    public function get_products()
    {   
        $this->db->select('product_id,product_name');
        $this->db->where('company_id',$this->company_id);
        $query=$this->db->get('products');
       return $results=$query->result();
    }
    public function get_taxe_value($tax_id)
    {   
        $this->db->where('tax_id',$tax_id);
        $query = $this->db->get('taxes');
        return $query->row();
    }
    public function get_customers()
    {   
        $this->db->where('company_id',$this->company_id);
        $this->db->where('status',1);
        $query=$this->db->get('customers');
        return $results=$query->result();
    }
    public function get_shipping_address($customer_id)
    {   
        $this->db->select('d.billing_address,d.shipping_address,d.state,d.city,d.pincode,c.customer_name,c.gstin,c.email_address,d.phone_number');
        $this->db->join('customers as c','c.customer_id=d.customer_id');
        $this->db->where('d.customer_id',$customer_id);
        $this->db->where('d.is_default',1);
        $query=$this->db->get('customers_detail as d');
        $row=$query->row();
        $session_data['billing_address']=$row->billing_address;
        $session_data['shipping_address']=$row->shipping_address;
        $session_data['state']=$row->state;
        $session_data['city']=$row->city;
        $session_data['pincode']=$row->pincode;
        $session_data['customer_name']=$row->customer_name;
        $session_data['gstin']=$row->gstin;
        $session_data['email_address']=$row->email_address;
        $session_data['phone_number']=$row->phone_number;
        $this->session->set_userdata("customer_details", $session_data);
        return $row;
    }
}
?>