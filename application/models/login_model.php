<?php 
class login_model extends CI_Model 
{   
    public function __construct()
    {
        parent :: __construct();
        $this->load->library('encrypt');
    }
	public function checkLogin($data)
    {   
        $username=$data['username'];
        $password=$data['password'];
        $company_id=$data['company_id'];
        //echo $this->encrypt->encode($password);
        $this->db->select('u.user_id,u.password,u.owner_name,c.company_id,c.company_name');
        $this->db->join('companies as c','c.user_id=u.user_id');
        $this->db->where('c.company_id',$company_id);
        $this->db->where('u.username',$username);
        $res=$this->db->get('users as u')->row();
        
        if(count($res) > 0)
        {       
            if($password==$this->encrypt->decode($res->password))
            {   $user_id=$res->user_id;
                $owner_name=$res->owner_name;
                $sessionData=array("isLogin"=>true,"user_id"=>$user_id,"owner_name"=>$owner_name,"company_id"=>$company_id);
                $this->session->set_userdata($sessionData);
                return array("status"=>1,"message"=>"Login Successfully");
            }else{
                return array("status"=>0,"message"=>"Password is incorrect!");
            }
            
        }else{
            return array("status"=>0,"message"=>"Invalid Credential!");
        }
    }   
    public function get_company($data)
    {
        $this->db->select('c.company_id as id,c.company_name as text');
        $this->db->join('users as u','u.user_id=c.user_id');
        $this->db->where('u.username',$data['username']);
        $results=$this->db->get('companies as c')->result();
        return json_encode($results);
    }
}
?>