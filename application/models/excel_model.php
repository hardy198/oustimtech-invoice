<?php 
class excel_model extends CI_Model 
{
	public function import_products($excelData)
    {   
        $action_date=date('Y-m-d H:i:s');
        foreach ($excelData as $data) 
        {   //Get UOM
            $this->db->where('LCASE(unit_name)',strtolower($data['uom']));
            $getUom=$this->db->get('uom')->row();
            if(count($getUom) > 0){
                $uom_id=$getUom->uom_id;
            }else{
                $this->db->insert('uom',array("unit_name"=>$data['uom'],"status"=>1));
                $uom_id=$this->db->insert_id();
            }

            //Get Tax
            $this->db->where('tax_rate',$data['tax']);
            $getTax=$this->db->get('taxes')->row();
            if(count($getTax) > 0){
                $tax_id=$getTax->tax_id;
            }else{
                $this->db->insert('taxes',array("company_id"=>$this->session->userdata('company_id'),"tax_name"=>$data['tax'].'% Tax',"tax_rate"=>$data['tax'],"cgst"=>$data['tax']/2,"sgst"=>$data['tax']/2,"igst"=>$data['tax'],"status"=>1));
                $tax_id=$this->db->insert_id();
            }

            $masterData=array("product_name"=>$data['product_name'],"product_desc"=>$data['description'],"hsn_scn"=>$data['hsn_scn'],
                              "uom_id"=>$uom_id,"tax_id"=>$tax_id,"company_id"=>$this->session->userdata('company_id'));

            //Check SKU
            $this->db->select('product_id');
            $this->db->where('sku',$data['sku']);
            $checkSKU=$this->db->get('products')->row();
            if(count($checkSKU) > 0){
                $masterData['modified_at']=$action_date;
                $this->db->where("sku",$checkSKU->product_id);
                $this->db->update('products',$masterData);
                $product_id=$checkSKU->product_id;
            }else{
                $masterData['sku']=$data['sku'];
                $masterData['created_at']=$action_date;
                $this->db->insert('products',$masterData);
                $product_id=$this->db->insert_id();
            }

            $batchData=array("batch_qty"=>$data['qty'],"remaining_qty"=>$data['qty'],"price"=>$data['price'],"batch_no"=>$data['batch_no'],"created_at"=>$action_date,"product_id"=>$product_id);

            $this->db->where("batch_no",$data['batch_no']);
            $this->db->delete('products_detail');

            $this->db->insert('products_detail',$batchData);
            
        }
    }
    
}
?>