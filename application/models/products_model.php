<?php 
class products_model extends CI_Model 
{
	
    public function get($action_id)
    {
        $this->db->where('product_id',$action_id);
        $query=$this->db->get('products');
        $result=$query->row();

        $this->db->where('product_id',$action_id);
        $query=$this->db->get('products_detail');
        $details=$query->result();
        return array("master_detail"=>$result,"details"=>$details);
    }
    public function delete($action_id)
    {
        $this->db->where('product_id',$action_id);
        $this->db->delete('products');

        $this->db->where('product_id',$action_id);
        $this->db->delete('products_detail');

        $returnarray=array("status"=>1,"message"=>"Product removed successfully");
        return $returnarray;
    }
    public function get_batches($action_id)
    {   
        $this->db->select('product_detail_id,batch_no');
        $this->db->where('product_id',$action_id);
        $this->db->where('remaining_qty > 0');
        $this->db->order_by('remaining_qty','ASC');
        $query=$this->db->get('products_detail');
        $batches=$query->result();
        return $batches;
    }
    public function get_product_detail($action_id)
    {
        $this->db->select('p.uom_id,p.tax_id,d.price,t.tax_rate,t.cgst,t.sgst,t.igst,p.hsn_scn');
        $this->db->join('products as p','p.product_id=d.product_id');
        $this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where('d.product_detail_id',$action_id);
        $rows=$this->db->get('products_detail as d')->row();
        return $rows;
    }
    public function get_all_product()
    {
        $this->db->select("p.product_name as Product Name,
                            (select price from products_detail where product_id=p.product_id and remaining_qty > 0 order by remaining_qty asc limit 1) as Price,
                            u.unit_name as UOM,
                            CONCAT(t.tax_rate,'','%') as Tax,
                            p.product_desc as Description,
                            sum(d.remaining_qty) as Quantity,
                            p.product_type as Type,
                            p.hsn_scn as HSN/SAC,
                            p.sku as SKU");
        $this->db->join('products_detail as d','d.product_id=p.product_id');
        $this->db->join('uom as u','u.uom_id=p.uom_id');
        $this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where('p.company_id',$this->session->userdata('company_id'));
        $this->db->group_by('d.product_id');
        $query=$this->db->get('products as p');
        $fields = $query->list_fields();
        $rows=$query->result();
        $returnarray=array("fields"=>$fields,"rows"=>$rows);
        return $returnarray;
    }

    public function lists()
    {	$searchData=$_POST;
        $searchQuery='p.company_id='.$this->session->userdata('company_id');
        if($searchData['product_id']!='')
        {
            $searchQuery .=' AND p.product_id='.$searchData['product_id'];
        }
        if($searchData['min_price']!='' && $searchData['max_price']=='')
        {
            $searchQuery .=' AND price <='.$searchData['min_price'];   
        }
        if($searchData['min_price']=='' && $searchData['max_price']!='')
        {
            $searchQuery .=' AND price >='.$searchData['max_price'];      
        }
        if($searchData['min_price']!='' && $searchData['max_price']!='')
        {
            $searchQuery .=' AND price BETWEEN '.$searchData['min_price'].' AND '.$searchData['max_price'];      
        }
        if($searchData['min_qty']!='' && $searchData['max_qty']=='')
        {
            $searchQuery .=' AND qty <='.$searchData['min_qty'];   
        }
        if($searchData['min_qty']=='' && $searchData['max_qty']!='')
        {
            $searchQuery .=' AND qty >='.$searchData['max_qty'];      
        }
        if($searchData['min_qty']!='' && $searchData['max_qty']!='')
        {
            $searchQuery .=' AND qty BETWEEN '.$searchData['min_qty'].' AND '.$searchData['max_qty'];      
        }
        if($searchData['sku']!='')
        {
            $searchQuery .=' AND p.sku="'.$searchData['sku'].'"';
        }
        
        $this->db->select("p.product_id,p.sku,p.product_name,p.product_desc,u.unit_name,CONCAT(t.tax_rate,'','%') as tax_rate,sum(d.remaining_qty) as qty,(select price from products_detail where product_id=p.product_id and remaining_qty > 0 order by remaining_qty asc limit 1) as price");
        $this->db->join('products_detail as d','d.product_id=p.product_id');
        $this->db->join('uom as u','u.uom_id=p.uom_id');
        $this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where($searchQuery);
        $this->db->group_by('d.product_id');
        $queryData1=$this->db->get('products as p');
        $toatlProductCount = $queryData1->num_rows();

    	$iTotalRecords = $toatlProductCount;
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		$orderCol=$_REQUEST['order'][0]['column'];
		$orderDir=$_REQUEST['order'][0]['dir'];
		$records = array();
		$records["data"] = array(); 

		if($orderCol==1) $orderField='p.product_name';
		elseif($orderCol==2) $orderField='p.product_desc';
		elseif($orderCol==3) $orderField='p.qty';
		elseif($orderCol==4) $orderField='d.price';
		elseif($orderCol==5) $orderField='u.unit_name';
		elseif($orderCol==6) $orderField='tax_rate';
		else{ $orderField='p.product_id'; $orderDir='desc';}


		$this->db->select("p.product_id,p.sku,p.product_name,p.product_desc,u.unit_name,CONCAT(t.tax_rate,'','%') as tax_rate,sum(d.remaining_qty) as qty,(select price from products_detail where product_id=p.product_id and remaining_qty > 0 order by remaining_qty asc limit 1) as price");
		$this->db->join('products_detail as d','d.product_id=p.product_id');
		$this->db->join('uom as u','u.uom_id=p.uom_id');
		$this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where($searchQuery);
		$this->db->order_by($orderField,$orderDir);
        $this->db->group_by('d.product_id');
        $this->db->limit($iDisplayLength,$iDisplayStart);
    	$query=$this->db->get('products as p');
        //echo $this->db->last_query();
    	$results=$query->result();

    	foreach($results as $result)
    	{
    		$records["data"][]=array($result->sku,$result->product_name,$result->product_desc,$result->qty,'<i class="fa fa-rupee-sign"></i> '. $result->price,$result->unit_name,$result->tax_rate,
    					'<a href="javascript:;" onclick=add_product('.$result->product_id.',"edit") class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>
						 <a href="javascript:;" onclick=add_product('.$result->product_id.',"view") class="btn default btn-xs green"><i class="fa fa-eye"></i> View </a>
						 <a href="javascript:;" onclick="delete_product('.$result->product_id.')" class="btn default btn-xs black"><i class="fa fa-trash-alt"></i> delete </a>'
    				);
    	}

    	$records["draw"] = $sEcho;
	    $records["recordsTotal"] = $iTotalRecords;
	    $records["recordsFiltered"] = $iTotalRecords;
	  	return $records;
	  

    }
    public function save($data)
    {	
        if(isset($data['action_id']) && $data['action_id']!=''){
            $mode='edit';
        }else{ $mode='new'; }

        if($mode=='edit'){
            $this->db->where('product_id!='.$data['action_id']);
        }
    	$this->db->where('sku',$data['sku']);
    	$count=$this->db->get('products')->num_rows();
    	if($count > 0){ 
    		$returnarray=array("status"=>0,"message"=>"Product SKU is already exist!");
    		return $returnarray;
    		exit();
    	}


    	$action_date=date('Y-m-d H:i:s');
        $product_name=$data['product_name'];
    	if(isset($data['expiry_date']) && $data['expiry_date']!='') $expiry_date=date('Y-m-d H:i:s',strtotime($data['expiry_date'])); else $expiry_date='';
    	$product_data=array(
    				"product_name"=>$product_name,
    				"product_desc"=>$data['product_desc'],
    				"sku"=>$data['sku'],
    				"hsn_scn"=>$data['hsn_scn'],
    				"uom_id"=>$data['uom_id'],
    				"tax_id"=>$data['tax_id'],
    				"product_type"=>$data['product_type'],
                    "company_id"=>$this->session->userdata('company_id'),
    				"expiry_date"=>$expiry_date
    			);
        if($mode=='edit')
        {
            $product_data['modified_by']=1;$product_data['modified_at']=$action_date;
            $this->db->where('product_id',$data['action_id']);
            $this->db->update('products',$product_data);
            $product_id=$data['action_id'];
        }else{
            $product_data['created_by']=1;$product_data['created_at']=$action_date;
            $this->db->insert('products',$product_data);
            $product_id=$this->db->insert_id();
        }

    	

        $product_detail_id=$data['product_detail_id'];

        for($i=0;$i<count($product_detail_id);$i++)
        {
            if(isset($data['price'][$i]) && $data['price'][$i]!=''){
                $product_detail_data=array(
                        "product_id"=>$product_id,
                        "batch_qty"=>$data['qty'][$i],
                        "batch_no"=>$data['batch_no'][$i],
                        "price"=>$data['price'][$i]
                );
                if($product_detail_id[$i]!='')
                {
                    $product_detail_data['modified_by']=1;$product_detail_data['modified_at']=$action_date;
                    $this->db->where('product_detail_id',$product_detail_id[$i]);
                    $this->db->update('products_detail',$product_detail_data);
                }else{
                    $product_detail_data['created_by']=1;$product_detail_data['created_at']=$action_date;$product_detail_data['remaining_qty']=$data['qty'][$i];
                    $this->db->insert('products_detail',$product_detail_data);
                }
            }
               
        }

        if($mode=='edit')
        {
            $returnarray=array("status"=>1,"message"=>"Product updated successfully","mode"=>$mode,"product_id"=>$product_id,"product_name"=>$product_name);
        }else{
            $returnarray=array("status"=>1,"message"=>"Product added successfully","mode"=>$mode,"product_id"=>$product_id,"product_name"=>$product_name);
        }
    	return $returnarray;
    }
}
?>