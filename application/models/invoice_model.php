<?php 
class invoice_model extends CI_Model 
{
    public function get_invoices()
    {   
        $this->db->select('product_id,product_name');
        $this->db->where('company_id',$this->session->userdata('company_id'));
        $query=$this->db->get('products');
       return $results=$query->result();
    }
    public function get($action_id)
    {
        $this->db->where('product_id',$action_id);
        $query=$this->db->get('products');
        $result=$query->row();

        $this->db->where('product_id',$action_id);
        $query=$this->db->get('products_detail');
        $details=$query->result();
        return array("master_detail"=>$result,"details"=>$details);
    }
    public function delete($action_id)
    {
        $this->db->where('product_id',$action_id);
        $this->db->delete('products');

        $this->db->where('product_id',$action_id);
        $this->db->delete('products_detail');

        $returnarray=array("status"=>1,"message"=>"Product removed successfully");
        return $returnarray;
    }
    public function get_all_product()
    {
        $this->db->select("p.product_name as Product Name,
                            (select price from products_detail where product_id=p.product_id and remaining_qty > 0 order by remaining_qty asc limit 1) as Price,
                            u.unit_name as UOM,
                            CONCAT(t.tax_rate,'','%') as Tax,
                            p.product_desc as Description,
                            sum(d.remaining_qty) as Quantity,
                            p.product_type as Type,
                            p.hsn_scn as HSN/SAC,
                            p.sku as SKU");
        $this->db->join('products_detail as d','d.product_id=p.product_id');
        $this->db->join('uom as u','u.uom_id=p.uom_id');
        $this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where('p.company_id',$this->session->userdata('company_id'));
        $this->db->group_by('d.product_id');
        $query=$this->db->get('products as p');
        $fields = $query->list_fields();
        $rows=$query->result();
        $returnarray=array("fields"=>$fields,"rows"=>$rows);
        return $returnarray;
    }

    public function lists()
    {	$searchData=$_POST;
        $searchQuery='p.company_id='.$this->session->userdata('company_id');
        if($searchData['product_id']!='')
        {
            $searchQuery .=' AND p.product_id='.$searchData['product_id'];
        }
        if($searchData['min_price']!='' && $searchData['max_price']=='')
        {
            $searchQuery .=' AND price <='.$searchData['min_price'];   
        }
        if($searchData['min_price']=='' && $searchData['max_price']!='')
        {
            $searchQuery .=' AND price >='.$searchData['max_price'];      
        }
        if($searchData['min_price']!='' && $searchData['max_price']!='')
        {
            $searchQuery .=' AND price BETWEEN '.$searchData['min_price'].' AND '.$searchData['max_price'];      
        }
        if($searchData['min_qty']!='' && $searchData['max_qty']=='')
        {
            $searchQuery .=' AND qty <='.$searchData['min_qty'];   
        }
        if($searchData['min_qty']=='' && $searchData['max_qty']!='')
        {
            $searchQuery .=' AND qty >='.$searchData['max_qty'];      
        }
        if($searchData['min_qty']!='' && $searchData['max_qty']!='')
        {
            $searchQuery .=' AND qty BETWEEN '.$searchData['min_qty'].' AND '.$searchData['max_qty'];      
        }
        if($searchData['sku']!='')
        {
            $searchQuery .=' AND p.sku="'.$searchData['sku'].'"';
        }
        
        $this->db->select("p.product_id,p.sku,p.product_name,p.product_desc,u.unit_name,CONCAT(t.tax_rate,'','%') as tax_rate,sum(d.remaining_qty) as qty,(select price from products_detail where product_id=p.product_id and remaining_qty > 0 order by remaining_qty asc limit 1) as price");
        $this->db->join('products_detail as d','d.product_id=p.product_id');
        $this->db->join('uom as u','u.uom_id=p.uom_id');
        $this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where($searchQuery);
        $this->db->group_by('d.product_id');
        $queryData1=$this->db->get('products as p');
        $toatlProductCount = $queryData1->num_rows();

    	$iTotalRecords = $toatlProductCount;
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		$orderCol=$_REQUEST['order'][0]['column'];
		$orderDir=$_REQUEST['order'][0]['dir'];
		$records = array();
		$records["data"] = array(); 

		if($orderCol==1) $orderField='p.product_name';
		elseif($orderCol==2) $orderField='p.product_desc';
		elseif($orderCol==3) $orderField='p.qty';
		elseif($orderCol==4) $orderField='d.price';
		elseif($orderCol==5) $orderField='u.unit_name';
		elseif($orderCol==6) $orderField='tax_rate';
		else{ $orderField='p.product_id'; $orderDir='desc';}


		$this->db->select("p.product_id,p.sku,p.product_name,p.product_desc,u.unit_name,CONCAT(t.tax_rate,'','%') as tax_rate,sum(d.remaining_qty) as qty,(select price from products_detail where product_id=p.product_id and remaining_qty > 0 order by remaining_qty asc limit 1) as price");
		$this->db->join('products_detail as d','d.product_id=p.product_id');
		$this->db->join('uom as u','u.uom_id=p.uom_id');
		$this->db->join('taxes as t','t.tax_id=p.tax_id');
        $this->db->where($searchQuery);
		$this->db->order_by($orderField,$orderDir);
        $this->db->group_by('d.product_id');
        $this->db->limit($iDisplayLength,$iDisplayStart);
    	$query=$this->db->get('products as p');
        //echo $this->db->last_query();
    	$results=$query->result();

    	foreach($results as $result)
    	{
    		$records["data"][]=array($result->sku,$result->product_name,$result->product_desc,$result->qty,'<i class="fa fa-rupee-sign"></i> '. $result->price,$result->unit_name,$result->tax_rate,
    					'<a href="javascript:;" onclick=open_popup('.$result->product_id.',"edit") class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>
						 <a href="javascript:;" onclick=open_popup('.$result->product_id.',"view") class="btn default btn-xs green"><i class="fa fa-eye"></i> View </a>
						 <a href="javascript:;" onclick="delete_product('.$result->product_id.')" class="btn default btn-xs black"><i class="fa fa-trash-alt"></i> delete </a>'
    				);
    	}

    	$records["draw"] = $sEcho;
	    $records["recordsTotal"] = $iTotalRecords;
	    $records["recordsFiltered"] = $iTotalRecords;
	  	return $records;
	  

    }
    public function save($data)
    {	
        if(isset($data['action_id']) && $data['action_id']!=''){
            $mode='edit';
        }else{ $mode='new'; }

        $action_date=date('Y-m-d H:i:s');
        $customer_id=$data['customer_id'];
        $company_id=$this->session->userdata('company_id');
        $payment_type_id=1;

        $cart_amounts=$this->session->userdata('cart_amounts');
        $shipping_charge=$this->session->userdata('shipping_charge');
        $items=$this->session->userdata('carts');

        $total_amount=$cart_amounts['cart_total'];
        $subtotal=$cart_amounts['subtotal_discount'];
        $discount=$cart_amounts['additional_discount'];
        $subtotal_discount=$cart_amounts['subtotal'];
        $shipping_cost=$shipping_charge['shipping_value'];
        $shipping_sgst=$shipping_charge['shipping_sgst'];
        $shipping_cgst=$shipping_charge['shipping_cgst'];
        $shipping_igst=$shipping_charge['shipping_igst'];
        $order_date=date('Y-m-d H:i:s',strtotime($data['order_date']));
        $customer_notes=$data['client_notes'];
        $internal_notes=$data['private_notes'];
        $created_by=$company_id;


        $orderData=array("customer_id"=>$customer_id,"company_id"=>$company_id,"payment_type_id"=>$payment_type_id,"total_amount"=>$total_amount,
                         "subtotal"=>$subtotal,"discount"=>$discount,"subtotal_discount"=>$subtotal_discount,"shipping_cost"=>$shipping_cost,
                         "shipping_sgst"=>$shipping_sgst,"shipping_cgst"=>$shipping_cgst,"shipping_igst"=>$shipping_igst,
                         "order_date"=>$order_date,"customer_notes"=>$customer_notes,"internal_notes"=>$internal_notes,"created_by"=>$created_by,"created_at"=>$action_date);

        /*Add Order*/
        $this->db->insert('orders',$orderData);
        $order_id=$this->db->insert_id();

        foreach ($items as $item) {
            $product_id=$item['product_id'];
            $product_desc=$item['desc'];
            $tax_rate=$item['tax_rate'];
            $tax_amount=$item['tax_amount'];
            $sgst_percentage=$item['sgst'];
            $cgst_percentage=$item['cgst'];
            $igst_percentage=$item['igst'];
            $cgst_amount=$item['cgst_value'];
            $sgst_amount=$item['sgst_value'];
            $igst_amount=0;
            $dicsount_type='percentage';
            $discount_amount=$item['discount_value'];
            $discount=$item['discount'];
            $qty=$item['qty'];
            $price=$item['price'];

            $itemData=array("order_id"=>$order_id,"product_id"=>$product_id,"product_desc"=>$product_desc,"tax_rate"=>$tax_rate,
                            "tax_amount"=>$tax_amount,"sgst_percentage"=>$sgst_percentage,"cgst_percentage"=>$cgst_percentage,
                            "igst_percentage"=>$igst_percentage,"cgst_amount"=>$cgst_amount,"sgst_amount"=>$sgst_amount,
                            "igst_amount"=>$igst_amount,"dicsount_type"=>$dicsount_type,"discount_amount"=>$discount_amount,"discount"=>$discount,
                            "qty"=>$qty,"price"=>$price,"created_by"=>$created_by,"created_at"=>$action_date);

            $this->db->insert('order_details',$itemData);
        }

        $document_number=$data['document_number'];
        $invoice_type_id=1;
        $invoive_date=date('Y-m-d H:i:s',strtotime($data['issue_date']));
        $due_date=date('Y-m-d H:i:s',strtotime($data['due_date']));
        $po_number=$data['po_number'];
        $invoice_desc=$data['invoice_description'];
        $customer_gstin='123456';
        $company_gstin='123456';
        $customer_place_supply=$data['city_id'];

        $invoiceData=array("customer_id"=>$customer_id,"company_id"=>$company_id,"order_id"=>$order_id,"document_number"=>$document_number,
                            "invoice_type_id"=>$invoice_type_id,"invoive_date"=>$invoive_date,"po_number"=>$po_number,"due_date"=>$due_date,
                            "invoice_desc"=>$invoice_desc,"invoice_value"=>$total_amount,"customer_gstin"=>$customer_gstin,
                            "company_gstin"=>$company_gstin,"customer_place_supply"=>$customer_place_supply,"created_by"=>$created_by,"created_at"=>$action_date);

        $this->db->insert('invoices',$invoiceData);

        if($mode=='edit')
        {
            $returnarray=array("status"=>1,"message"=>"Product updated successfully","mode"=>$mode);
        }else{
            $returnarray=array("status"=>1,"message"=>"Product added successfully","mode"=>$mode);
        }
        $items=$this->session->unset_userdata('carts');
        $shipping_charge=$this->session->unset_userdata('shipping_charge');
        $cart_discount=$this->session->unset_userdata('cart_discount');
        $customer_details=$this->session->unset_userdata('customer_details');
        $cart_amounts=$this->session->unset_userdata('cart_amounts');
    	return $returnarray;
    }
}
?>