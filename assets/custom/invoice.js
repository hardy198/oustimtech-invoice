var grid='';
/*var product_count=1;
var sub_total=new Array();
var total_discount=new Array();
var total_cart_value=new Array();*/
jQuery(document).ready(function() {       

    $(document).on('ifChanged','.shipping-tick',function(){
        
        if($(".shipping-tick").is(':checked')){
            var shipping_cost=jQuery('.shipping-cost').val();
            var shipping_tax=jQuery('#shipping_tax').val();
            if(shipping_cost!='' && shipping_cost > 0){
                update_shipping({shipping_cost:shipping_cost,shipping_tax:shipping_tax});    
            }
            jQuery('.shipping-options,.shipping-total').show();
        }else{
            update_shipping({shipping_cost:0,shipping_tax:0});
            jQuery('.shipping-options,.shipping-total').hide();
        }
        
    });
    $(document).on('change','.shipping-cost',function(){
        if($(".shipping-tick").is(':checked')){
            var shipping_cost=jQuery(this).val();
            var shipping_tax=jQuery('#shipping_tax').val();
            if(shipping_cost!='' && shipping_cost > 0){
                update_shipping({shipping_cost:shipping_cost,shipping_tax:shipping_tax});    
            }
        }
    });
    $(document).on('change','#shipping_tax',function(){
        if($(".shipping-tick").is(':checked')){
            var shipping_cost=jQuery('.shipping-cost').val();
            var shipping_tax=jQuery(this).val();
            if(shipping_cost!='' && shipping_cost > 0){
                update_shipping({shipping_cost:shipping_cost,shipping_tax:shipping_tax});    
            }
        }
    });
    
    $(document).on('ifChanged','.discount-tick',function(){
        jQuery('.discount-options').toggle();
        if($(".discount-tick").is(':checked')){
           // $('#invoice-save-frm input[name="discount"]').prop("disabled", true);
            var discount_cost=jQuery('.discount_all_value').val();
            var discount_type=jQuery('#discount_type').val();
            if(discount_cost!='' && discount_cost > 0){
                update_cart_discount({discount_cost:discount_cost,discount_type:discount_type});    
            }
        }else{
            //$('#invoice-save-frm input[name="discount"]').prop("disabled", false);
            update_cart_discount({discount_cost:0,discount_type:''});
        }
    });

    $(document).on('change','.discount_all_value',function(){
        if($(".discount-tick").is(':checked')){
            var discount_cost=jQuery(this).val();
            var discount_type=jQuery('#discount_type').val();
            if(discount_cost!='' && discount_cost > 0){
                update_cart_discount({discount_cost:discount_cost,discount_type:discount_type});    
            }
        }
    });
    $(document).on('change','#discount_type',function(){
        if($(".discount-tick").is(':checked')){
            var discount_cost=jQuery('.discount_all_value').val();
            var discount_type=jQuery(this).val();
            if(discount_cost!='' && discount_cost > 0){
                update_cart_discount({discount_cost:discount_cost,discount_type:discount_type});    
            }
        }
    });
    $(document).on('click','.preview-content',function(){
        if($('#invoice-save-frm').valid()){
            if(jQuery('#invoice_products tbody tr').length > 0)
            {
                $.ajax({
                    url: base_url+'invoices/preview_invoice',
                    type: 'post',
                    success: function (result)
                    {   
                        $('body').modalmanager('loading');
                        $('.preview-invoice-body').html(result);
                        $('#invoice-preview-modal').modal('show');
                    }
                });
            }else{
                alertify.alert('No products are available.');
                return false;
            }
        }
    });

    

    // initiate layout and plugins
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Demo.init(); // init demo features
    
    grid = new Datatable();

        grid.init({
            src: $("#datatable_products"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { 
                
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "serverSide": true,
                "processing": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": base_url+"products/lists", // ajax source
                    "type": "POST",
                    "data": function ( d ) {
                        d.product_id = $('.product-serachfrm [name="product_id"]').val();
                        d.product_type = $('.product-serachfrm [name="product_type"]').val();
                        d.min_price = $('.product-serachfrm [name="min_price"]').val();
                        d.max_price = $('.product-serachfrm [name="max_price"]').val();
                        d.sku = $('.product-serachfrm [name="sku"]').val();
                        d.min_qty = $('.product-serachfrm [name="min_qty"]').val(); 
                        d.max_qty = $('.product-serachfrm [name="max_qty"]').val(); 
                    }
                },
                "order": [
                    [1, "asc"]
                ]// set first column as a default sort by asc
            }
        });

    UIExtendedModals.init();
    jQuery('.select2').select2();
    
});
function open_popup(action_id,mode)
{   $(".custom-select2").select2("close");
    $('body').modalmanager('loading');
    $.ajax({
            url: base_url+'invoices/get',
            type: 'post',
            data: {action_id:action_id,mode:mode},
            success: function (result)
            {   
                $('body').modalmanager('loading');
                $('.target').html(result);
                $('#saving-modal').modal('show');
                $('.date-picker').datepicker({rtl: Metronic.isRTL(),orientation: "left",autoclose: true});
                apply_custom_select2();
                handleiCheck();
                validate_modal();
            }
        });
}
function validate_modal()
{
    jQuery('#invoice-save-frm').validate({errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                submitHandler: function (form) {
                    
                }
    });
}
function delete_product(action_id)
{   
    alertify.set({labels: {
            ok: "Yes",
            cancel: "No"
    }});
    alertify.confirm("Are you sure you want to delete this product?", function (e) {
        if (e)
        {
            $.ajax({
                url: base_url+'products/delete',
                type: 'post',
                data: {action_id:action_id},
                dataType: 'json',
                success: function (result)
                {
                    if(result.status==1)
                    {
                        alertify.success(result.message);
                        grid.getDataTable().ajax.reload();
                    }else{
                        alertify.error(result.message);
                    }
                }
            });
        } 
    });
    
}

$('.search-btn').click(function(){
   grid.getDataTable().ajax.reload(); 
});

$(document).on('change','#invoice-save-frm #product_id',function(){
        jQuery('#invoice_batch_no').html('<option value=""></option>');
        var product_id=$(this).val();
        $.ajax({
            url: base_url+'products/get_batches',
            type: 'post',
            data: {action_id:product_id},
            dataType: 'json',
            success: function (result)
            {
                var options='';
                jQuery.each(result, function(i, item) {
                    options +='<option value="'+result[i].product_detail_id+'">'+result[i].batch_no+'</option>';
                });
                jQuery('#invoice_batch_no').html(options);
                jQuery("#invoice_batch_no").select2("val", result[0].product_detail_id);
                get_product_detail(result[0].product_detail_id);
            }
        });
});
$(document).on('change','#invoice-save-frm #invoice_batch_no',function(){
    var product_detail_id=$(this).val();
    get_product_detail(product_detail_id);   
});
function get_product_detail(product_detail_id)
{
    $.ajax({
            url: base_url+'products/get_product_detail',
            type: 'post',
            data: {action_id:product_detail_id},
            dataType: 'json',
            success: function (result)
            {
                $('#invoice-save-frm #uom_id').val(result.uom_id);
                $('#invoice-save-frm #price').val(result.price);
                $('#invoice-save-frm select[name="tax_id"]').val(result.tax_id);
                $('#invoice-save-frm input[name="hidden_product_tax_rate"]').val(result.tax_rate);
                $('#invoice-save-frm input[name="hidden_product_cgst"]').val(result.cgst);
                $('#invoice-save-frm input[name="hidden_product_sgst"]').val(result.sgst);
                $('#invoice-save-frm input[name="hidden_product_igst"]').val(result.igst);
                $('#invoice-save-frm input[name="hidden_product_hsn_sac"]').val(result.hsn_scn);
            }
        });
}

function add_products_to_list()
{   
    var hsn_scn=$('#invoice-save-frm input[name="hidden_product_hsn_sac"]').val();
    var product_id=$('#invoice-save-frm select[name="product_id"]').select2('data').id;
    var product_name=$('#invoice-save-frm select[name="product_id"]').select2('data').text;
    var desc=$('#invoice-save-frm input[name="invoice_description"]').val();
    var uom_id=$('#invoice-save-frm select[name="uom_id"]').val();
    var uom=$('#invoice-save-frm select[name="uom_id"] :selected').text();
    var qty=$('#invoice-save-frm input[name="qty"]').val();
    var price=$('#invoice-save-frm input[name="price"]').val();
    var discount=$('#invoice-save-frm input[name="discount"]').val();
    var batch_id=$('#invoice-save-frm select[name="batch_no"] :selected').val();
    var batch_no=$('#invoice-save-frm select[name="batch_no"] :selected').text();
    var tax_id=$('#invoice-save-frm select[name="tax_id"] :selected').val();
    var rowId=$('#invoice-save-frm input[name="rowId"]').val();

    if(product_id=='')
    {
        alertify.alert('Please select product.');
        return false;
    }
    else if(batch_id=='')
    {
        alertify.alert('Please select Batch Number.');
        return false;
    }
    else if(qty=='')
    {   
        alertify.alert('Quantity can not be blank.');
        return false;
    }
    else if(price=='')
    {
        alertify.alert('Price can not be blank.');
        return false;
    }
    else if(tax_id=='')
    {
        alertify.alert('Please select Tax.');
        return false;
    }

    $.ajax({
            url: base_url+'invoices/add_to_cart',
            type: 'post',
            data: {product_id:product_id,product_name:product_name,desc:desc,uom_id:uom_id,uom:uom,qty:qty,price:price,discount:discount,
                   hsn_scn:hsn_scn,batch_id:batch_id,batch_no:batch_no,tax_id:tax_id,rowId:rowId},
            success: function (result)
            {   
                jQuery('.invoice_products_target').html(result);
                jQuery('.invoice_add_product_frm input,.invoice_add_product_frm select').val('');
                jQuery(".invoice_add_product_frm .select2,.product-custom-select2").select2("val", "");
                $('#invoice-save-frm input[name="rowId"]').val('');
                $('.add-to-cart label').text('Add');
                update_cart_total();
            }
        });
}
function edit_cart(rowId)
{
    $.ajax({
            url: base_url+'invoices/get_cart_item',
            type: 'post',
            data: {rowId:rowId},
            dataType: 'json',
            success: function (result)
            {   
                var product_id=result.product_id;
                var batch_id=result.batch_id;
                var desc=result.desc;
                var uom=result.uom_id;
                var qty=result.qty;
                var price=result.price;
                var discount=result.discount;
                var tax_id=result.tax_id;
                edit_cart_value(product_id,batch_id,desc,uom,qty,price,discount,tax_id,rowId);
            }
        });
}
function remove_cart_item(rowId,that)
{
    $.ajax({
            url: base_url+'invoices/remove_cart_item',
            type: 'post',
            data: {rowId:rowId},
            success: function (result)
            {   
                jQuery(that).parents('tr').remove();
                update_cart_total();
            }
        });
}
function edit_cart_value(product_id,batch_id,desc,uom,qty,price,discount,tax_id,rowId)
{
    $('#invoice-save-frm select[name="product_id"]').select2("val", product_id);
    $('#invoice-save-frm select[name="batch_no"]').val(batch_id);
    $('#invoice-save-frm input[name="invoice_description"]').val(desc);
    $('#invoice-save-frm select[name="uom_id"]').val(uom);
    $('#invoice-save-frm input[name="qty"]').val(qty);
    $('#invoice-save-frm input[name="price"]').val(price);
    $('#invoice-save-frm input[name="discount"]').val(discount);
    $('#invoice-save-frm select[name="tax_id"]').val(tax_id);
    $('#invoice-save-frm input[name="rowId"]').val(rowId);
    $('.add-to-cart label').text('Update');
}

function update_cart_total()
{
    $.ajax({
            url: base_url+'invoices/get_cart_total',
            type: 'post',
            success: function (result)
            {   
                jQuery('.cart-total-sec').html(result);
            }
        });
}
function update_shipping(postData)
{
     $.ajax({
            url: base_url+'invoices/add_shipping',
            type: 'post',
            data: postData,
            success: function (result)
            {   
                update_cart_total();
            }
        });
}
function update_cart_discount(postData)
{
     $.ajax({
            url: base_url+'invoices/add_cart_discount',
            type: 'post',
            data: postData,
            success: function (result)
            {   
                update_cart_total();
            }
        });
}
function get_shipping_address(selectObject)
{
    var customer_id=selectObject.value;
    if(customer_id!='')
    {   
        $.ajax({
            url: base_url+'invoices/get_shipping_address',
            type: 'post',
            dataType: 'json',
            data: {action_id:customer_id},
            success: function (result)
            {   console.log(result);
                var shipStr ='<label><b>Ship To:</b></label>';
                    shipStr +='<p>'+result.shipping_address+'</p>';
                    shipStr +='<p>'+result.state+', '+result.city+'-'+result.pincode+'</p>';
               $('.shipTo').html(shipStr);
            }
        });
    }else{
        $('.shipTo').html('');
    }
    
}

function generate_pdf()
{
    $.ajax({
            url: base_url+'invoices/generate_pdf',
            type: 'post',
            success: function (result)
            {   
                //update_cart_total();
            }
        });
}

function save_invoice()
{
    $.ajax({
            url: base_url+'invoices/save',
            type: 'post',
            dataType: 'json',
            data: $('#invoice-save-frm').serialize(),
            success: function (result)
            {   
                location.reload();
            }
        });
}
