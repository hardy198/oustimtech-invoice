jQuery(document).ready(function(){
	apply_dependencies();
    apply_custom_select2();
	
	$(document).on('click','.add-batch',function(){
    $('.batch-clone-target').append($('.batch-clone-src').html());
	});
	$(document).on('click','.remove-batch',function(){
	    $(this).parents('.batch-row').remove();
	});
	$(document).on('change','#product_type',function(){
		if(jQuery(this).val()=='goods')
		{
			jQuery('#hsn_scn_label').text('HSN');
		}else{
			jQuery('#hsn_scn_label').text('SCN');
		}
	});	
    
});
function apply_custom_select2(){
    jQuery('.product-custom-select2').select2({
        formatNoMatches: function() {
            return '<a href="javascript:;" class="btn btn-default green select-add-product" onclick=add_product("","add")><i class="fa fa-plus"></i><span class="hidden-480">New Product </span></a>';
        },
    });
    jQuery('.customer-custom-select2').select2({
        formatNoMatches: function() {
            return '<a href="javascript:;" class="btn btn-default green select-add-product" onclick=add_customer("","add")><i class="fa fa-plus"></i><span class="hidden-480">New Customer </span></a>';
        },
    });
}
function handleiCheck()
{
    $('.icheck').each(function() {
            var checkboxClass = $(this).attr('data-checkbox') ? $(this).attr('data-checkbox') : 'icheckbox_minimal-grey';
            var radioClass = $(this).attr('data-radio') ? $(this).attr('data-radio') : 'iradio_minimal-grey';

            if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1) {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass,
                    insert: '<div class="icheck_line-icon"></div>' + $(this).attr("data-label")
                });
            } else {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass
                });
            }
        });
}
function apply_dependencies(){
	$("#date-mask").inputmask("d/m/y", {"placeholder": "dd/mm/yyyy"});
	$('.date-picker').datepicker({rtl: Metronic.isRTL(),orientation: "left",autoclose: true});
    $('.default-select2').select2();
}
function add_product(action_id,mode,place)
{   if(place=='diffrentplace')
        var product_name=$(".product-custom-select2").data("select2").search[0].value;
    else
        var product_name='';
    $(".product-custom-select2").select2("close");
    $('body').modalmanager('loading');
	$.ajax({
            url: base_url+'products/get',
            type: 'post',
            data: {action_id:action_id,mode:mode,product_name:product_name},
            success: function (result)
            {   
                $('body').modalmanager('loading');
                $('.target').html(result);
                $('#saving-product-modal').modal('show');
                apply_dependencies();
                validate_product_modal(place);
            }
        });
}
function validate_product_modal(place)
{
    jQuery('#product-save-frm').validate({errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                submitHandler: function (form) {
                    $.ajax({
                        url: base_url+'products/save',
                        type: 'post',
                        data: jQuery('#product-save-frm').serialize(),
                        dataType: 'json',
                        success: function (result)
                        {
                            if(result.status==1)
                            {
                                alertify.success(result.message);
                                grid.getDataTable().ajax.reload();
                                if(result.mode=='new'){document.getElementById("product-save-frm").reset();}
                                if(place=='diffrentplace')
                                {	var product_id=result.product_id;
                                	var product_name=result.product_name;
                                	$("select.product-custom-select2").append("<option value='"+product_id+"' selected>"+product_name+"</option>").trigger('change');
                                	$('#saving-product-modal').modal('hide');
                                }
                            }else{
                                alertify.error(result.message);
                            }
                        }
                    });
                }
    });
}