var grid='';
jQuery(document).ready(function() {       
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
	
	grid = new Datatable();

        grid.init({
            src: $("#datatable_products"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { 
                
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "serverSide": true,
                "processing": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": base_url+"products/lists", // ajax source
                    "type": "POST",
                    "data": function ( d ) {
                        d.product_id = $('.product-serachfrm [name="product_id"]').val();
                        d.product_type = $('.product-serachfrm [name="product_type"]').val();
                        d.min_price = $('.product-serachfrm [name="min_price"]').val();
                        d.max_price = $('.product-serachfrm [name="max_price"]').val();
                        d.sku = $('.product-serachfrm [name="sku"]').val();
                        d.min_qty = $('.product-serachfrm [name="min_qty"]').val(); 
                        d.max_qty = $('.product-serachfrm [name="max_qty"]').val(); 
                    }
                },
                "order": [
                    [1, "asc"]
                ]// set first column as a default sort by asc
            }
        });

	UIExtendedModals.init();
	
	
});

function delete_product(action_id)
{	
	alertify.set({labels: {
            ok: "Yes",
            cancel: "No"
    }});
    alertify.confirm("Are you sure you want to delete this product?", function (e) {
        if (e)
        {
            $.ajax({
	            url: base_url+'products/delete',
	            type: 'post',
	            data: {action_id:action_id},
	            dataType: 'json',
	            success: function (result)
	            {
	             	if(result.status==1)
	             	{
	             		alertify.success(result.message);
	             		grid.getDataTable().ajax.reload();
	             	}else{
	             		alertify.error(result.message);
	             	}
	            }
	        });
        } 
    });
	
}
function upload_file()
{
    $('.import-file').click();
}
function export_products()
{
    $.ajax({
        url: base_url+'products/exportproduct',
        type: 'post',
        success: function (result)
        {
            
        }
    });
}
$(document).on('change','.import-file',function(e){
    var file = $(this)[0].files[0];
    var upload = new Upload(file);

    // maby check size or type here with upload.getSize() and upload.getType()

    // execute upload
    upload.doUpload();
});

var Upload = function (file) {
    this.file = file;
};

Upload.prototype.getType = function() {
    return this.file.type;
};
Upload.prototype.getSize = function() {
    return this.file.size;
};
Upload.prototype.getName = function() {
    return this.file.name;
};
Upload.prototype.doUpload = function () {
    var that = this;
    var formData = new FormData();

    // add assoc key values, this will be posts values
    formData.append("excelfile", this.file, this.getName());
    formData.append("upload_file", true);

    $.ajax({
        type: "POST",
        url: "products/importproduct",
        success: function (data) {
            if(data.status){
                $('#import-modal').modal('hide');
                alertify.success(data.message);
                grid.getDataTable().ajax.reload();
            }else{
                alertify.error(data.message);
            }
        },
        error: function (error) {
            alertify.error('Something went wrong! Please try again.');
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        timeout: 60000
    });
};


$('.search-btn').click(function(){
   grid.getDataTable().ajax.reload(); 
});